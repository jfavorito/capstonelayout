# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(username: "admin", password: "admin", admin: true)
User.create(username: "c201810123", password: "coordinator", admin: false, role: 2)
Tempval.create(subjtotake: 10, default_subjtopass: 8)

Program.create(program_title: "Master of Science in Computer Science")
Program.create(program_title: "Master in Information Technology")
Program.create(program_title: "Master in Computer Graphics and Animation")
Program.create(program_title: "Master in Information Systems")

Curriculum.create(curr_type: "NEW")
Curriculum.create(curr_type: "OLD")
Curriculum.create(curr_type: "SHIFTEE")

Schoolyear.create(startyr: 2014, endyear: 2015)
Semester.create(sem_name: "1st")
Semester.create(sem_name: "2nd")
Semester.create(sem_name: "Summer")

Discount.create(disc_name: "None", disc_value: 0)

Contpan.create(site_name: "AdNU ICTC GPIS", semester_id: 1, schoolyear_id: 1, college: "Graduate School", department: "Graduate School", enforce: false)

Classtype.create(ctype_name: "Tutorial", ctype_count: 1, countmax: 8)
Classtype.create(ctype_name: "Regular", ctype_count: 9, countmax: 40)

Appearance.create(color: 'blue', sec_color: 'green')