class CreateSchoolyears < ActiveRecord::Migration[5.0]
  def change
    create_table :schoolyears do |t|
      t.integer :startyr
      t.integer :endyear

      t.timestamps
    end
  end
end
