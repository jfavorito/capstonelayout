class CreateContpans < ActiveRecord::Migration[5.0]
  def change
    create_table :contpans do |t|
      t.string :site_name
      t.integer :semester_id

      t.timestamps
    end
  end
end
