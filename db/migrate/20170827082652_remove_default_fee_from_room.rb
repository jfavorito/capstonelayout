class RemoveDefaultFeeFromRoom < ActiveRecord::Migration[5.0]
  def change
    remove_column :rooms, :default_fee, :string
  end
end
