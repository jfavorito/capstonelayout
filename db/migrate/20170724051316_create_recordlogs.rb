class CreateRecordlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :recordlogs do |t|
      t.datetime :date_added
      t.integer :history_id
      t.integer :offering_id

      t.timestamps
    end
  end
end
