class AddUsernameToFaculty < ActiveRecord::Migration[5.0]
  def change
    add_column :faculties, :username, :string
  end
end
