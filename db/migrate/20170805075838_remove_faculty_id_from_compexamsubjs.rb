class RemoveFacultyIdFromCompexamsubjs < ActiveRecord::Migration[5.0]
  def change
    remove_column :compexamsubjs, :faculty_id, :integer
  end
end
