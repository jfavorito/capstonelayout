class AddYearToStudent < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :year, :string
  end
end
