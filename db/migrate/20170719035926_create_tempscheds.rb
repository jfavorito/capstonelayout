class CreateTempscheds < ActiveRecord::Migration[5.0]
  def change
    create_table :tempscheds do |t|
      t.time :t_start
      t.time :t_end
      t.string :t_days

      t.timestamps
    end
  end
end
