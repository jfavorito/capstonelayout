class CreateFees < ActiveRecord::Migration[5.0]
  def change
    create_table :fees do |t|
      t.string :fee_name

      t.timestamps
    end
  end
end
