class CreateFaculties < ActiveRecord::Migration[5.0]
  def change
    create_table :faculties do |t|
      t.string :f_id
      t.string :f_fname
      t.string :f_mname
      t.string :f_lname
      t.string :f_type
      t.string :degree

      t.timestamps
    end
  end
end
