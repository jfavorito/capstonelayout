class AddSecColorToAppearance < ActiveRecord::Migration[5.0]
  def change
    add_column :appearances, :sec_color, :string
  end
end
