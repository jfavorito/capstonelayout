class CreateSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :schedules do |t|
      t.time :time_start
      t.time :time_end
      t.integer :offering_id

      t.timestamps
    end
  end
end
