class AddSemesterIdToCompexamsubj < ActiveRecord::Migration[5.0]
  def change
    add_column :compexamsubjs, :semester_id, :integer
  end
end
