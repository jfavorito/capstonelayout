class AddOfferingIdToCompexamsubj < ActiveRecord::Migration[5.0]
  def change
    add_column :compexamsubjs, :offering_id, :integer
  end
end
