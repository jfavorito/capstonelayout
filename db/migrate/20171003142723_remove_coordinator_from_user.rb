class RemoveCoordinatorFromUser < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :coordinator, :boolean
  end
end
