class RemoveSchoolyearIdFromCompexamsubjs < ActiveRecord::Migration[5.0]
  def change
    remove_column :compexamsubjs, :schoolyear_id, :integer
  end
end
