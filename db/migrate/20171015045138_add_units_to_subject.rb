class AddUnitsToSubject < ActiveRecord::Migration[5.0]
  def change
    add_column :subjects, :units, :decimal
  end
end
