class CreateTempvals < ActiveRecord::Migration[5.0]
  def change
    create_table :tempvals do |t|
      t.integer :default_subjtopass

      t.timestamps
    end
  end
end
