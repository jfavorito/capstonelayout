class RemoveSemesterIdFromCompexamsubjs < ActiveRecord::Migration[5.0]
  def change
    remove_column :compexamsubjs, :semester_id, :integer
  end
end
