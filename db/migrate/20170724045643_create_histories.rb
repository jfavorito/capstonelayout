class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.datetime :date_enlisted
      t.integer :student_id

      t.timestamps
    end
  end
end
