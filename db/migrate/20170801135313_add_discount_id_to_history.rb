class AddDiscountIdToHistory < ActiveRecord::Migration[5.0]
  def change
    add_column :histories, :discount_id, :integer
  end
end
