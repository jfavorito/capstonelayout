class CreateCompexamsubjs < ActiveRecord::Migration[5.0]
  def change
    create_table :compexamsubjs do |t|
      t.integer :compexam_id
      t.integer :faculty_id
      t.boolean :passed
      t.datetime :datetaken

      t.timestamps
    end
  end
end
