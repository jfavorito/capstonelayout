class CreateClasstypes < ActiveRecord::Migration[5.0]
  def change
    create_table :classtypes do |t|
      t.string :ctype_name
      t.integer :ctype_count

      t.timestamps
    end
  end
end
