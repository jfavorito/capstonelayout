class AddImageToAppearance < ActiveRecord::Migration[5.0]
  def change
    add_column :appearances, :image, :string
  end
end
