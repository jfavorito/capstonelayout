class AddDefaultFeeToSubject < ActiveRecord::Migration[5.0]
  def change
    add_column :subjects, :default_fee, :integer
  end
end
