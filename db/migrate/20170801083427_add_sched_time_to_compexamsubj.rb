class AddSchedTimeToCompexamsubj < ActiveRecord::Migration[5.0]
  def change
    add_column :compexamsubjs, :sched_time, :time
  end
end
