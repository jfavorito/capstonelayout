class CreateCompexams < ActiveRecord::Migration[5.0]
  def change
    create_table :compexams do |t|
      t.datetime :date_applied
      t.integer :subjtotake
      t.integer :subjtopass
      t.decimal :amount
      t.string :or_no
      t.integer :student_id

      t.timestamps
    end
  end
end
