class CreateSpecializations < ActiveRecord::Migration[5.0]
  def change
    create_table :specializations do |t|
      t.integer :program_id
      t.string :spec_name

      t.timestamps
    end
  end
end
