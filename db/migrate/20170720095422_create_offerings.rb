class CreateOfferings < ActiveRecord::Migration[5.0]
  def change
    create_table :offerings do |t|
      t.string :section
      t.integer :slots_available
      t.integer :slots_taken
      t.integer :faculty_id
      t.integer :fee_id
      t.integer :room_id
      t.integer :schoolyear_id
      t.integer :semester_id
      t.integer :subject_id
      t.integer :classtype_id

      t.timestamps
    end
  end
end
