class AddDaysToSchedule < ActiveRecord::Migration[5.0]
  def change
    add_column :schedules, :days, :string
  end
end
