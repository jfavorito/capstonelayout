class CreateDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :discounts do |t|
      t.string :disc_name
      t.decimal :disc_value

      t.timestamps
    end
  end
end
