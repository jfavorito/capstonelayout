class AddCoordinatorToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :coordinator, :boolean
  end
end
