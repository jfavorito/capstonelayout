class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :stud_id
      t.string :s_fname
      t.string :s_mname
      t.string :s_lname
      t.string :gender
      t.string :status
      t.integer :curriculum_id
      t.integer :specialization_id

      t.timestamps
    end
  end
end
