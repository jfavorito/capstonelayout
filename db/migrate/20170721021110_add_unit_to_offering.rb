class AddUnitToOffering < ActiveRecord::Migration[5.0]
  def change
    add_column :offerings, :unit, :decimal
  end
end
