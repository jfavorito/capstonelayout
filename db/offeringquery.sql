select subject_code, subject_title, section, slots_taken, f_fname ||" "|| f_mname ||" "|| f_lname "Teacher", startyr, endyear, sem_name
from offerings as A 
join subjects as B
join faculties as C 
join schoolyears as D
join semesters as E
where A.subject_id = B.id
and A.faculty_id = C.id
and A.schoolyear_id = D.id
and A.semester_id = E.id