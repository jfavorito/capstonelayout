# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171021004440) do

  create_table "appearances", force: :cascade do |t|
    t.string   "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "image"
    t.string   "sec_color"
  end

  create_table "classtypes", force: :cascade do |t|
    t.string   "ctype_name"
    t.integer  "ctype_count"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "countmax"
  end

  create_table "compexams", force: :cascade do |t|
    t.datetime "date_applied"
    t.integer  "subjtotake"
    t.integer  "subjtopass"
    t.decimal  "amount"
    t.string   "or_no"
    t.integer  "student_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "temp"
  end

  create_table "compexamsubjs", force: :cascade do |t|
    t.integer  "compexam_id"
    t.boolean  "passed"
    t.datetime "datetaken"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "sched_date"
    t.time     "sched_time"
    t.integer  "offering_id"
    t.integer  "examiner"
  end

  create_table "contpans", force: :cascade do |t|
    t.string   "site_name"
    t.integer  "semester_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "schoolyear_id"
    t.string   "college"
    t.string   "department"
    t.string   "full_name"
    t.boolean  "enforce"
    t.integer  "cetotake"
    t.integer  "cetopass"
  end

  create_table "curriculums", force: :cascade do |t|
    t.string   "curr_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "discounts", force: :cascade do |t|
    t.string   "disc_name"
    t.decimal  "disc_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faculties", force: :cascade do |t|
    t.string   "f_id"
    t.string   "f_fname"
    t.string   "f_mname"
    t.string   "f_lname"
    t.string   "f_type"
    t.string   "degree"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "username"
    t.string   "password_digest"
  end

  create_table "fans", force: :cascade do |t|
    t.string   "name"
    t.string   "stan"
    t.integer  "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fees", force: :cascade do |t|
    t.string   "fee_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histories", force: :cascade do |t|
    t.datetime "date_enlisted"
    t.integer  "student_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "discount_id"
    t.integer  "sy"
    t.integer  "sem"
  end

  create_table "offerings", force: :cascade do |t|
    t.string   "section"
    t.integer  "slots_available"
    t.integer  "slots_taken"
    t.integer  "faculty_id"
    t.integer  "fee_id"
    t.integer  "room_id"
    t.integer  "schoolyear_id"
    t.integer  "semester_id"
    t.integer  "subject_id"
    t.integer  "classtype_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.decimal  "unit"
  end

  create_table "programs", force: :cascade do |t|
    t.string   "program_title"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "recordlogs", force: :cascade do |t|
    t.datetime "date_added"
    t.integer  "history_id"
    t.integer  "offering_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "room_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.time     "time_start"
    t.time     "time_end"
    t.integer  "offering_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "days"
    t.string   "tcon"
    t.string   "rcon"
  end

  create_table "schoolyears", force: :cascade do |t|
    t.integer  "startyr"
    t.integer  "endyear"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "semesters", force: :cascade do |t|
    t.string   "sem_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sies", force: :cascade do |t|
    t.string   "syear"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specializations", force: :cascade do |t|
    t.integer  "program_id"
    t.string   "spec_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.string   "stud_id"
    t.string   "s_fname"
    t.string   "s_mname"
    t.string   "s_lname"
    t.string   "gender"
    t.string   "status"
    t.integer  "curriculum_id"
    t.integer  "specialization_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "year"
    t.string   "username"
    t.string   "password_digest"
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "subject_code"
    t.string   "subject_title"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "default_fee"
    t.decimal  "units"
  end

  create_table "tempscheds", force: :cascade do |t|
    t.time     "t_start"
    t.time     "t_end"
    t.string   "t_days"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tempvals", force: :cascade do |t|
    t.integer  "default_subjtopass"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "subjtotake"
  end

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.boolean  "admin"
    t.integer  "role"
  end

end
