# AdNU ICTC Graduate Program Enlistment and Comprehensive Exam Application with Business Report

AdNU ICTC GPIS is a web- based information system developed using Ruby on Rails for the Ateneo de Naga University Information and Communications Technology Center Graduate Program. Even though the system is tailored to support the business processes and reports needed by the AdNU ICTC, there are features in the system that supports customizability. 

## Prerequisites

Something that can run a Rails 5 app

## Installation

1. Fork this application by typing `git clone git@gitlab.com:jfavorito/capstonelayout.git` on the terminal or command line (Linux and Windows)
2. Navigate to the directory of the cloned application
3. type `bundle install` in the terminal/command line
4. type `rake db:migrate` in the terminal/command line
5. type `rake db:seed` to load the needed initial records in the database

## Usage

1. type `rails s` to start the application
2. In your native browser go to the url localhost:3000/sessions/new
3. In the login page, the default account information for the coordinator involves the username ***admin*** and password which is also ***admin***.
4. For the teacher account, the username is ***t*** + ***teacher's id number*** and the password will be the teacher's ***surname*** + the ***first four characters of the first name***. For example if the teacher's id number is 201410746 and the name of the teacher is Peter Panda, then the username and password will be ***t201410746*** and ***PandaPete*** respectively.
5. For the student account, the username is ***s*** + ***student's id number*** and the password will be the teacher's ***surname*** + the ***first four characters of the first name***. For example if the teacher's id number is 201410747 and the name of the teacher is Perry Platypus, then the username and password will be ***s201410747*** and ***PlatypusPerr*** respectively.

## Issues

A common issue with regards to this Rails application is the bcrypt related errors produced after `bundle install`, this case is more often in the Windows Operating System. In order to resolve this bcrypt must be uninstalled and reinstalled all over again, in order to do this, the lines of code must be executed in the command line:

1. `gem uninstall bcrypt`
2. Select option 3 from the choices.
3. `gem uninstall bcrypt-ruby`
4. `gem install bcrypt platform=ruby`

## Credits

The use of sessions and modals were made easier by the [Rails Application](https://github.com/MCHLPortal/mchl-portal) developed by the group of Closa, Concina, Moreno, and Rabago.


