# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# $ -> 
#   $.rails.allowAction = (link) ->
#     return true unless link.attr('data-confirm')
#     $.rails.showConfirmDialog(link)
#     false

#   $.rails.confirmed = (link) ->
#     link.removeAttr('data-confirm')
#     link.trigger('click.rails')

#   $.rails.showConfirmDialog = (link) ->
#     message = link.attr 'data-confirm'
#     html = """
#            <div class="modal" id="confirmationDialog">
#              <div class="modal-dialog">
#                <div class="modal-content">
#                 <div class="modal-body">
#                   <div class="row">
#                      <div class="col-md-9"
#                       <p>#{message}</p>
#                      </div>
#                      <div class="col-md-3">
#                        <a data-dismiss="modal" class="btn btn-default">Cancel</a>
#                        <a data-dismiss="modal" class="btn btn-primary confirm" onclick="setTimeout('history.go(0);');">Ok</a>
#                     </div>
#                    </div>
#                  </div>
#                </div>
#              </div>
#            </div>
#            """
#     $(html).modal()
#     $('#confirmationDialog .confirm').on('click', $.rails.confirmed())

jQuery ->
  $('#datatabs').dataTable( {
    sPaginationType: "simple_numbers",
    select: true
    select: {
          style: 'single'
    }

  } );
