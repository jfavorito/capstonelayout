class FacultyloadingPdf < Prawn::Document

	#  Line 5-46
	#  Main Function for generating the Faculty Loading in PDF format
	def initialize(faculty)
		super()
		@faculty = faculty
		@sy = Contpan.first.schoolyear_id
		@sem = Contpan.first.semester_id
		header								#  Calls the header Function
		subheader							#  Calls the subheader Function

		#  Line 15-45
		#  Creates table for each faculty with additional information above it
		@faculty.each do |faculty|
			move_down 20
			@count = Offering.where(faculty_id: faculty.id).where("slots_taken > ?", 0).where(schoolyear_id: @sy).where(semester_id: @sem).count
            @totalunits = 0
            @of = Offering.where(faculty_id: faculty.id).where("slots_taken > ?", 0).where(schoolyear_id: @sy).where(semester_id: @sem)
            @of.each do |off|
              	@totalunits = @totalunits.to_f + off.unit.to_f
           	end
			text "#{faculty.f_lname}, #{faculty.f_fname} #{faculty.f_mname[0]}.", :style => :bold, :align => :left, :size => 10
			text "Highest Academic Qualification: #{faculty.degree}", :align => :left, :size => 10
			text "No. of Preparations: #{@count}", :size => 10
			text "No. of Units: #{@totalunits}", :size => 10
			text "Total Pay: ", :size => 10
			@arrTypes = Array.new
			@of.each do |o|
               if !@arrTypes.include? o.classtype_id
               		@arrTypes << o.classtype_id
               end
            end
            @arrTypes.each do |a|
            	@totalunits = 0
            	@of.each do |off|
            		if a == off.classtype_id
            			@totalunits = @totalunits + off.unit
            		end
            	end
            	text "#{@totalunits} (#{Classtype.find(a).ctype_name})"
            end
						move_down 10
            table_content								#  Calls the table_content Function
		end
	end

	#  Line 50-54
	#  Creates the header for the Faculty Loading Report
	def header
		img = "#{Rails.root}/app/assets/images/pdfHeader.png"
	   image img, :position => :center, :width => 600
		text "FACULTY LOADING REPORT", :align => :center, :size => 11, :style => :bold
	end

	#  Line 58-71
	#  Creates the subheader for the Faculty Loading Report
	def subheader
		text "Department: #{Contpan.first.department}"
		text "Semester: #{Semester.find(Contpan.first.semester_id).sem_name}"
		text "College: #{Contpan.first.college}"
		@syear = Schoolyear.find(Contpan.first.schoolyear_id)
		text "School Year: #{@syear.startyr}-#{@syear.endyear}"
		@program = Program.all
		text "Courses: "
		@count = 1
		@program.each do |p|
			text "#{@count}. #{p.program_title}"
			@count = @count + 1
		end
	end

	#  Line 75-82
	#  Calls the product_rows function and displays it as a table
	def table_content
		table(product_rows, :header => true,
												:column_widths => {0 => 70, 1 => 185, 2 => 55, 3 => 40, 4 => 100, 5 => 50},
												:cell_style => { :padding => [5, 0, 5, 5], :size => 10 }, :width => 550, :position => :center) do
			row(0).font_style   = :bold
			row(0).align = :center
		end
	end

	#  Line 86-91
	#  Collects all subject load's information for the faculties
	def product_rows
		[["SUBJ CODE","SUBJ TITLE", "SECTION", "UNITS", "TIME", "DAYS", "CLASS SIZE"]] +
		@of.map do |s|
			[code(s.subject_id),title(s.subject_id),s.section,s.unit,time(s.id), days(s.id), s.slots_taken]
		end
	end

	#  Line 93-98
	#  Identifies the load's subject code
	def code(subj)
		@subj = subj
		return Subject.find(@subj).subject_code
	end

	#  Line 102-105
	#  Identifies the load's subject title
	def title(subj)
		@subj = subj
		return Subject.find(@subj).subject_title
	end

	#  Line 109-113
	#  Identifies the load's assigned schedule
	def time(offering)
		@t = offering
		@timestart = Schedule.find_by(offering_id: @t).time_start.strftime("%I:%M %p")
		return @timestart
	end

	#  Line 117-123
	#  Identifies the load's assigned days
	def days(offering)
		@t = offering
		@temp1 = Schedule.find_by(offering_id: @t).days.gsub(/[",0]/,'')
		@temp2 = @temp1.gsub(/"|\[|\]/, '')
		@temp3 = @temp2.delete(' ')
		return @temp3
	end
end
