class RoomfeePdf < Prawn::Document

	#  Line 5-11
	#  Main Function for generating the list of room fees in PDF format
	def initialize(offering)
		super()
		@offering = offering
		header								#  Calls the header Function
		move_down 20
		table_content					#  Calls the table_content Function
	end

	#  Line 15-21
	#  Creates the header for the list of room fees
	def header
		img = "#{Rails.root}/app/assets/images/pdfHeader.png"
	   	image img, :position => :center, :width => 600
		text "List of Subjects Offered", :align => :center, :size => 11, :style => :bold
		text "With and Without Laboratory Fees", :align => :center, :size => 11, :style => :bold
		text "Second Semester, SY 2016-2017", :align => :center, :size => 11, :style => :bold
	end

	#  Line 25-32
	#  Calls the product_rows function and displays it as a table
	def table_content
		table(product_rows, :header => true,
												:column_widths => { 1 => 70, 2 => 200 },
												:cell_style => { :padding => [5, 0, 5, 5], :size => 10 }, :width => 550, :position => :center) do
			row(0).font_style   = :bold
			row(0).align = :center
		end
	end

	#  Line 36-42
	#  Collects all of the Subjects assigned to its designated room
	def product_rows
		@count = 0
		[["No", "Subject", "Description", "Units", "Room", "Laboratory Fee"]] +
		@offering.map do |o|
			[@count = @count + 1, code(o.subject_id), title(o.subject_id), o.unit, room(o.room_id), fee(o.fee_id)]
		end
	end

	#  Line 46-49
	#  Identifies the subject code
	def code(subject)
		@subject = subject
		return Subject.find(@subject).subject_code
	end

	#  Line 53-56
	#  Identifies the subject title
	def title(subject)
		@subject = subject
		return Subject.find(@subject).subject_title
	end

	#  Line 60-63
	#  Identifies the subject's assigned room
	def room(room_id)
		@room_id = room_id
		return Room.find(@room_id).room_name
	end

	#  Line 67-70
	#  Identifies the assigned room's type of fee
	def fee(ct)
		@type = ct
		return Fee.find(@type).fee_name
	end
end
