class ClasslistPdf < Prawn::Document

	#  Line 5-18
	#  Main Function for generating Class List in PDF format
	def initialize(offering, students)
		super()
		@offering = offering
		@students = students
		@semester = Semester.find(@offering.semester_id)
		@schoolyear = Schoolyear.find(@offering.schoolyear_id)
		@schedule = Schedule.find_by(offering_id: @offering.id)
		@faculty = Faculty.find(@offering.faculty_id)
		@subject = Subject.find(@offering.subject_id)
		@room = Room.find(@offering.room_id)
		header								#  Calls the header Function
		info									#  Calls the info function
		table_content					#  Calls the table_content function
	end

	#  Line 22-27
	#  Creates the header for Class List
	def header
		text "ATENEO DE NAGA UNIVERSITY",:align => :center, :size => 16, :style => :bold
        text "#{@semester.sem_name} Sem, School Year #{@schoolyear.startyr} - #{@schoolyear.endyear}",:align => :center, :size => 14
        pad_bottom(10) { text "CLASSLIST" , :align => :center, :size => 14 }
				stroke_horizontal_rule
	end

	#  Line 31-52
	#  Displays the general information of the Class
	def info
		@tempday = @schedule.days.gsub(/[",0]/,'')
        @tempday1 = @tempday.gsub(/"|\[|\]/, '')
        @days = @tempday1.delete(' ')
        if @faculty.f_fname == "TBA"
        	@teacher = "TBA"
        else
			@teacher = @faculty.f_fname + " " + @faculty.f_mname[0] + ". " + @faculty.f_lname
        end
        @type = Classtype.find(@offering.classtype_id).ctype_name


		move_down 10
		text "College                     :  #{Contpan.first.college}", :align => :left, :size => 10
		text "Class                        :  #{@subject.subject_code}-#{@subject.subject_title} #{@offering.section}", :align => :left, :size => 10
		text "Schedule                  :  #{@schedule.time_start.strftime("%I:%M %p")}-#{@schedule.time_end.strftime("%I:%M %p")} #{@days} #{@room.room_name} #{@teacher}", :align => :left, :size => 10
		text "Department              :  #{Contpan.first.department}", :align => :left, :size => 10
		text "Credit Units              :  #{@offering.unit}", :align => :left, :size => 10
		text "Number of Students :  #{@students.count} Type: #{@type}", :align => :left, :size => 10
		pad_bottom(0) { text "  " }
		pad_bottom(30) { stroke_horizontal_rule }
	end

	#  Line 56-58
	#  Calls the product_rows function and displays it as a table
	def table_content
		table product_rows, :position => :center
	end

	#  Line 62-68
	#  Collects all students that are enrolled in the class
	def product_rows
		@count = 0
		[["No","Student ID", "Name", "Year", "Course", "Status", "Date Enrolled"]] +
			@students.map do |s|
				[@count = @count + 1, s.stud_id, fullname(s.id), s.year, program(s.specialization_id), status(s.id), date(s.id)]
			end
	end

	#  Line 72-85
	#  Identifies each student's program track
	def program(sid)
		@sid = sid
		@spec = Specialization.find(sid)
		@program = Program.find(@spec.program_id)
		@title = @program.program_title
		@length = @title.length
		@str = ""
		@length.times do |l|
			if @title[l] == @title[l].upcase && @title[l] != " "
				@str = @str + @title[l]
			end
		end
		return @str
	end

	#  Line 89-101
	#  Identifies the student's name and displays the student's last name first
	def fullname(sid)
		@sid = sid
		@student = Student.find(@sid)
		@lname = @student.s_lname + ", "
		@fname = @student.s_fname + " "
		if @student.s_mname == ""
			@fullname = @lname + @fname
		else
			@mname = @student.s_mname[0] + "."
			@fullname = @lname + @fname + @mname
		end
		return @fullname
	end

	#  Line 105-114
	#  Identifies if a student is currently enrolled or not
	def status(sid)
		@sid = sid
		@student = Student.find(sid)
		@stat
		if @student.status == "Enrolled"
			@stat = "[E]"
		else
			@stat = "[NE]"
		end
	end

	#  Line 118-123
	#  Identifies each student's enrollment date
	def date(sid)
		@sid = sid
		@history = History.find_by(student_id: @sid, sy: @schoolyear, sem: @semester)
		return @history.date_enlisted.strftime("%a, %b %d %Y").to_s
	end
end
