class OfferingPdf < Prawn::Document

	#  Line 5-21
	#  Main Function for generating the list of Subject Offerings in PDF format
	def initialize(offering)
		super(:page_size => "LEGAL",:page_layout => :landscape)
		@offering = offering
		@tempsy = Contpan.first.schoolyear_id
		@start = Schoolyear.find(@tempsy).startyr
		@end = Schoolyear.find(@tempsy).endyear
		@sy = @start.to_s + "-" + @end.to_s
		@tempsem = Contpan.first.semester_id
		@sem = Semester.find(@tempsem)
		if @sem.sem_name != "Summer"
			@sem = @sem.sem_name + " Semester"
		else
			@sem = @sem.sem_name
		end
		header								#  Calls the header Function
		table_content 				#  Calls the table_content Function
	end

	#  Line 25-30
	#  Creates the header for the list of Subject Offerings
	def header
		text "ATENEO DE NAGA UNIVERSITY", :align => :center, :size => 16
		text "GRADUATE SCHOOL", :align => :center, :size => 16
		text "Subject Offerings", :align => :center, :size => 16
		text "#{@sem} School Year #{@sy}", :align => :center, :size => 16
	end

	#  Line 34-40
	#  Calls the product_rows function and displays it as a table
	def table_content
		table(product_rows, :column_widths => { 1 => 50, 2 => 200 },
												:position => :center) do
			row(0).font_style = :bold
			row(0).align = :center
		end
	end

	#  Line 44-49
	#  Collects all of the Subjects being offered and displays in a form of table
	def product_rows
		[["Subject Code", "Section", "Title", "Units","Time/Days", "Room", "Teacher", "Max Slots", "Available","Taken"]] +
		@offering.map do |o|
			[code(o.subject_id), o.section, title(o.subject_id), o.unit, sched(o.id), room(o.room_id), faculty(o.faculty_id), o.slots_available + o.slots_taken, o.slots_available, o.slots_taken]
		end
	end

	#  Line 53-56
	#  Identifies all of the subject code being offered
	def code(subject)
		@subj = subject
		return Subject.find(@subj).subject_code
	end

	#  Line 60-63
	#  Identifies all of the subject title being offered
	def title(subject)
		@subj = subject
		return Subject.find(@subj).subject_title
	end

	#  Line 67-78
	#  Identifies all subject offering's assigned schedule
	def sched(schedule)
		@off = schedule
		@schedule = Schedule.where(offering_id: @off)
		@stArr = ""
		@schedule.each do |s|
			@timestart = Schedule.find(s.id).time_start.strftime("%I:%M %p")
			@timeend = Schedule.find(s.id).time_end.strftime("%I:%M %p")
			@temp1 = Schedule.find(s.id).days.gsub(/[",0]/,'')
			@temp2 = @temp1.gsub(/"|\[|\]/, '')
			@temp3 = @temp2.delete(' ')
			@stArr = @stArr + " " + @timestart + " - " + @timeend + " " + @temp3
		end

		return @stArr
	end

	#  Line 85-102
	#  Identifies all subject offering's assigned faculty
	def faculty(facultyid)
		@faculty = facultyid
		if Faculty.find(@faculty).f_fname == "TBA"
			@full = "TBA"
		else
			if Faculty.find(@faculty).f_mname == ""
				@last = Faculty.find(@faculty).f_lname
				@first = Faculty.find(@faculty).f_fname
				@full = @last.to_s + ", " + @first.to_s + " "
			else
				@last = Faculty.find(@faculty).f_lname
				@first = Faculty.find(@faculty).f_fname
				@mid = Faculty.find(@faculty).f_mname
				@full = @last.to_s + ", " + @first.to_s + " " + @mid.to_s + "."
			end
		end
		return @full
	end

	#  Line 106-109
	#  Identifies all subject offering's assigned room
	def room(roomid)
		@room = roomid
		return Room.find(@room).room_name
	end
end
