class TuitionPdf < Prawn::Document

	#  Line 5-11
	#  Main Function for generating the list of student's discount in PDF format
	def initialize(students)
		super()
		@students = students
		header								#  Calls the header Function
		move_down 20
		table_content					#  Calls the table_content Function
	end

	#  Line 15-22
	#  Creates the header for the list of student's discount
	def header
		img = "#{Rails.root}/app/assets/images/pdfHeader.png"
	   	image img, :position => :center, :width => 600
		@date = Time.now.to_date.strftime("%B %d, %Y")
		text "List of Students with Tuition Fee Discounts", :align => :center, :size => 11
		text "as of #{@date}", :align => :center, :size => 11
		text "2nd Semester, School Year 2016-2017", :align => :center, :size => 11
	end

	#  Line 26-33
	#  Calls the product_rows function and displays it as a table
	def table_content
		table(product_rows, :header => true,
												:column_widths => {0 => 100, 2 => 100, 3 => 120},
												:cell_style => { :padding => [5, 0, 5, 5], :size => 10 }, :width => 550, :position => :center) do
			row(0).font_style   = :bold
			row(0).align = :center
		end
	end

	#  Line 37-42
	#  Collects all of the students with discount
	def product_rows
		[["ID NUMBER", "NAME", "COURSE", "DISCOUNT"]] +
		@students.map do |s|
			[s.stud_id, fullname(s.id), course(s.specialization_id), discount(s.id)]
		end
	end

	#  Line 46-59
	#  Identifies each student's name and displays their last name first
	def fullname(sid)
		@id = sid
		@stud = Student.find(sid)
		@fname = @stud.s_fname.upcase + " "
		@mname = @stud.s_mname[0]
		@lname = @stud.s_lname.upcase + " "
		if @stud.s_mname == ""
			@fullname = @lname + @fname
		else
			@mname = @stud.s_mname[0] + "."
			@fullname = @lname + @fname + @mname
		end
		return @fullname
	end

	#  Line 63-68
	#  Identifies each student's program track
	def course(specId)
		@specId = specId
		@spec = Specialization.find(@specId)
		@program = Program.find(@spec.program_id)
		return @program.program_title
	end

	#  Line 72-80
	#  Identifies each student's type of discount
	def discount(sid)
		@id = sid
		@sem = Contpan.first.semester_id
		@sy = Contpan.first.schoolyear_id
		@his = History.find_by(student_id: @id, sem: @sem, sy: @sy)
		@discount = Discount.find(@his.discount_id)

		return @discount.disc_name
	end
end
