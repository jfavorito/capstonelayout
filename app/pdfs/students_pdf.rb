class StudentsPdf < Prawn::Document

	#  Line 5-11
	#  Main Function for generating the list of enrolled students in PDF format
	def initialize(students)
		super()
		@students = students
		header								#  Calls the header Function
		move_down 20
		table_content					#  Calls the table_content Function
	end

	#  Line 15-36
	#  Creates the header for the list of enrolled students
	def header
		if Contpan.first.enforce == true
			@sem = Contpan.first.semester_id
			@sy = Contpan.first.schoolyear_id
			@docSem = Semester.find(@sem).sem_name
			@tempSy = Schoolyear.find(@sy)
			@docSy = @tempSy.startyr.to_s + "-" + @tempSy.endyear.to_s
			text "ATENEO DE NAGA UNIVERSITY", :align => :center, :size => 14, :style => :bold
			text "GRADUATE SCHOOL", :align => :center, :size => 11, :style => :bold
			text "List of Officially Enrolled Students", :align => :center, :size => 11, :style => :bold
			text "#{@docSem} Semester, School Year #{@docSy}", :align => :center, :size => 11, :style => :bold
			pad_bottom(0) { text "  " }
			pad_bottom(30) { stroke_horizontal_rule }
		else
			text "ATENEO DE NAGA UNIVERSITY", :align => :center, :size => 14, :style => :bold
			text "GRADUATE SCHOOL", :align => :center, :size => 11, :style => :bold
			text "List of Officially Enrolled Students", :align => :center, :size => 11, :style => :bold
			text "Summer, School Year 2016-2017", :align => :center, :size => 11, :style => :bold
			pad_bottom(0) { text "  " }
			pad_bottom(30) { stroke_horizontal_rule }
		end
	end

	#  Line 40-47
	#  Calls the product_rows function and displays it as a table
	def table_content
		table(product_rows, :header => true,
												:column_widths => { 1 => 70, 2 => 180 },
												:cell_style => { :padding => [5, 0, 5, 5], :size => 10 }, :width => 450, :position => :center) do
			row(0).font_style   = :bold
			row(0).align = :center
		end
	end

	#  Line 51-57
	#  Collects all of the officially enrolled students
	def product_rows
		@c = 0
		[["NO", "STUDENT_ID", "NAME", "COURSE", "MAJOR"]] +
		@students.map do |s|
			[@c = @c + 1, s.stud_id, fullname(s.id), course(s.specialization_id), major(s.specialization_id)]
		end
	end

	#  Line 61-68
	#  Identifies each student's name and displays their last names first
	def fullname(sid)
		@id = sid
		@first = Student.find(sid).s_fname
		@mid = Student.find(sid).s_mname
		@last = Student.find(sid).s_lname
		@full = @last + ", " + @first + " " + @mid
		return @full
	end

	#  Line 71-7
	#  Identifies each student's program track
	def course(spec)
		@spec = spec
		@program_id = Specialization.find(@spec).program_id
		@program = Program.find(@program_id).program_title
		return @program
	end

	#  Line 81-85
	#  Identifies each student's specialization
	def major(sid)
		@sid = sid
		@spec = Specialization.find(@sid).spec_name
		return @spec
	end
end
