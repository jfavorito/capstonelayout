class EnlistmentPdf < Prawn::Document

	#  Line 5-27
	#  Main Function for generating Enlisted Subjects in PDF format
	def initialize(student, records)
		super()
		@student = student
		@offering = records
		@tempsy = Contpan.first.schoolyear_id
		@start = Schoolyear.find(@tempsy).startyr
		@end = Schoolyear.find(@tempsy).endyear
		@sy = @start.to_s + "-" + @end.to_s
		@tempsem = Contpan.first.semester_id
		@sem = Semester.find(@tempsem)
		if @sem.sem_name != "Summer"
			@sem = @sem.sem_name + " Semester"
		else
			@sem = @sem.sem_name
		end
		if @student.s_mname == ""
			@fullname = @student.s_fname + " " +  @student.s_lname
		else
			@fullname = @student.s_fname + " " + @student.s_mname[0] + ". " + @student.s_lname
		end
		header								#  Calls the header Function
		table_content					#  Calls the table_content function
	end

	#  Line 31-36
	#  Creates the header for the Enlisted Subjects
	def header
		text "Enlisted Subjects", :align => :center, :size => 16
		text "ATENEO DE NAGA UNIVERSITY", :align => :center, :size => 16
		text "#{@sem} School Year #{@sy}", :align => :center, :size => 16
		text "#{@fullname}", :align => :center, :size => 16
	end

	#  Line 40-42
	#  Calls the product_rows function and displays it as a table
	def table_content
		table product_rows
	end

	#  Line 46-51
	#  Collects all of the student's enlisted subjects and displays it as a table
	def product_rows
		[["Subject Code", "Section", "Title", "Units","Time/Days", "Room", "Teacher"]] +
		@offering.map do |o|
			[code(o.subject_id), o.section, title(o.subject_id), o.unit, sched(Schedule.find_by(offering_id: o.id).id), room(o.room_id), faculty(o.faculty_id)]
		end
	end

	#  Line 55-58
	#  Identifies each enlisted subject's code
	def code(subject)
		@subj = subject
		return Subject.find(@subj).subject_code
	end

	#  Line 62-65
	#  Identifies each enlisted subject's title
	def title(subject)
		@subj = subject
		return Subject.find(@subj).subject_title
	end

	#  Line 69-78
	#  Identifies each enlisted subject's schedule
	def sched(schedule)
		@sched = schedule
		@timestart = Schedule.find(@sched).time_start.strftime("%I:%M %p")
		@timeend = Schedule.find(@sched).time_end.strftime("%I:%M %p")
		@temp1 = Schedule.find(@sched).days.gsub(/[",0]/,'')
		@temp2 = @temp1.gsub(/"|\[|\]/, '')
		@temp3 = @temp2.delete(' ')
		@schedfinal = @timestart + " - " + @timeend + " " + @temp3
		return @schedfinal
	end

	#  Line 82-93
	#  Identifies each enlisted subject's assigned faculty
	def faculty(facultyid)
		@faculty = facultyid
		@last = Faculty.find(@faculty).f_lname
		@first = Faculty.find(@faculty).f_fname
		@mid = Faculty.find(@faculty).f_mname
		if @first == "TBA"
			@full = "TBA"
		else
			@full = @last.to_s + ", " + @first.to_s + " " + @mid.to_s + "."
		end
		return @full
	end

	#  Line 97-100
	#  Identifies each enlisted subject's assigned room
	def room(roomid)
		@room = roomid
		return Room.find(@room).room_name
	end
end
