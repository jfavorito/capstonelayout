class EnrollmentdataPdf < Prawn::Document

	#  Line 5-12
	#  Main Function for generating the enrollment data in PDF format
	def initialize(studentIDs)
		super()
		@studentIDs = studentIDs
		header								#  Calls the header Function
		move_down 20
		table_content					#  Calls the 1st table
		table_another					#  Calls the 2nd table
	end

	#  Line 16-22
	#  Creates the header for the Enrollment data
	def header
		img = "#{Rails.root}/app/assets/images/pdfHeader.png"
	   image img, :position => :center, :width => 600
		move_down 30
		text "Updated Enrollment Data", :align => :center, :size => 11, :style => :bold
		text "1st Semester, SY 2016-2017", :align => :center, :size => 11, :style => :bold
	end

	#  Line 26-34
	#  Calls the product_rows function and displays it as the 1st table
	def table_content
			table(product_rows, :header => true,
													:column_widths => { 1 => 70, 2 => 70, 3 => 70, 4 => 70},
													:cell_style => { :padding => [5, 0, 5, 5], :size => 10 }, :width => 450, :position => :center) do
				row(0).font_style   = :bold
				row(0).align = :center
				column(0).font_style = :bold
			end
	end

	#  Line 38-45
	#  Collects the tally of New, Old, Shiftee by the student's program track
	def product_rows
		@program = Program.all
		@counter = 0
		[["COURSE", "NEW", "OLD", "SHIFTEE", "TOTAL"]] +
		@program.map do |p|
			[p.program_title, countNew(p.id), countOld(p.id), countShift(p.id), total(p.id)]
		end
	end

	#  Line 49-54
	#  Calls the testAdd function and displays it as the 2nd table
	def table_another
		table(testAdd, :column_widths => { 1 => 70, 2 => 70, 3 => 70, 4 => 70},
									 :cell_style => { :padding => [5, 0, 5, 5], :size => 10 }, :width => 450, :position => :center) do
			row(0).font_style   = :bold
		end
	end

	#  Line 58-60
	#  Collects all of the total count of New, Old, and Shiftee Students
	def testAdd
		[["TOTAL",totalNew(),totalOld(),totalShift(),totalOverall()]]
	end

	#  Line 64-82
	#  Counts all New Students
	def totalNew()
		@curriculum = Curriculum.find_by(curr_type: "NEW").id
		@program = Program.all
		@total = 0
		@program.each do |p|
			@tempArray = Array.new
			@specialization = Specialization.where(program_id: p.id)
			@specialization.each do |sp|
				@tempArray << sp.id
			end
			@students = Student.where(id: @studentIDs).where(specialization_id: @tempArray)
			@students.each do |st|
				if st.curriculum_id == @curriculum
					@total = @total + 1
				end
			end
		end
		return @total
	end

	#  Line 86-104
	#  Counts all Old Students
	def totalOld()
		@curriculum = Curriculum.find_by(curr_type: "OLD").id
		@program = Program.all
		@total = 0
		@program.each do |p|
			@tempArray = Array.new
			@specialization = Specialization.where(program_id: p.id)
			@specialization.each do |sp|
				@tempArray << sp.id
			end
			@students = Student.where(id: @studentIDs).where(specialization_id: @tempArray)
			@students.each do |st|
				if st.curriculum_id == @curriculum
					@total = @total + 1
				end
			end
		end
		return @total
	end

	#  Line 108-126
	#  Counts all Shiftee Students
	def totalShift()
		@curriculum = Curriculum.find_by(curr_type: "SHIFTEE").id
		@program = Program.all
		@total = 0
		@program.each do |p|
			@tempArray = Array.new
			@specialization = Specialization.where(program_id: p.id)
			@specialization.each do |sp|
				@tempArray << sp.id
			end
			@students = Student.where(id: @studentIDs).where(specialization_id: @tempArray)
			@students.each do |st|
				if st.curriculum_id == @curriculum
					@total = @total + 1
				end
			end
		end
		return @total
	end

	#  Line 130-132
	#  Summarizes the total number of the New, Old, and Shiftee students
	def totalOverall()
		return totalNew() + totalOld + totalShift()
	end

	#  Line 137-152
	#  Counts all New Students by Program track
	def countNew(pId)
		@id = pId
		@curriculum = Curriculum.find_by(curr_type: "NEW")
		@specialization = Specialization.where(program_id: @id)
		@specArray = Array.new
		@specialization.each do |spec|
			@specArray << spec.id
		end
		@student = Student.where(id: @studentIDs).where(specialization_id: @specArray)
		@count = 0
		@student.each do |s|
			if s.curriculum_id == @curriculum.id
				@count = @count + 1
			end
		end
		return @count
	end

	#  Line 156-172
	#  Counts all Old Students by Program track
	def countOld(pId)
		@id = pId
		@curriculum = Curriculum.find_by(curr_type: "OLD").id
		@specialization = Specialization.where(program_id: @id)
		@specArray = Array.new
		@specialization.each do |spec|
			@specArray << spec.id
		end
		@student = Student.where(id: @studentIDs).where(specialization_id: @specArray)
		@count = 0
		@student.each do |s|
			if s.curriculum_id == @curriculum
				@count = @count + 1
			end
		end
		return @count
	end

	#  Line 176-192
	#  Counts all Shiftee Students by Program track
	def countShift(pId)
		@id = pId
		@curriculum = Curriculum.find_by(curr_type: "SHIFTEE").id
		@specialization = Specialization.where(program_id: @id)
		@specArray = Array.new
		@specialization.each do |spec|
			@specArray << spec.id
		end
		@student = Student.where(id: @studentIDs).where(specialization_id: @specArray)
		@count = 0
		@student.each do |s|
			if s.curriculum_id == @curriculum
				@count = @count + 1
			end
		end
		return @count
	end

	#  Line 196-199
	#  Summarizes the total of Old, New, and Shiftee Students by Program track
	def total(pId)
		@id = pId
		return countOld(@id) + countNew(@id) + countShift(@id)
	end
end
