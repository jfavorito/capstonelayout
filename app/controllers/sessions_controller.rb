class SessionsController < ApplicationController
  def new
  end

  def create
  	user = Student.find_by(username: params[:session][:username])
  	if user && user.authenticate(params[:session][:password])
  		type="student"
  		session[:user_id] = user.id
		session[:user_type] = type
  		redirect_to '/students/dashboard'
  	else
  		user = Faculty.find_by(username: params[:session][:username])
  		if user && user.authenticate(params[:session][:password])
  			type = "faculty"
  			session[:user_id] = user.id
			  session[:user_type] = type
  			redirect_to '/teachers/dashboard'
  		else
  			user = User.find_by(username: params[:session][:username])
  			if user && user.authenticate(params[:session][:password]) && user.role == 1
  				type="admin"
  				session[:user_id] = user.id
				  session[:user_type] = type
  				redirect_to '/users/home'
        elsif user && user.authenticate(params[:session][:password])
          type="coordinator"
          session[:user_id] = user.id
          session[:user_type] = type
          redirect_to '/coordinators/dashboard'
  			else
  				redirect_to login_path, danger: "Invalid Username or Password"
  			end
  		end
  	end
  end

  def destroy
  	session.delete(:user_id)
	  @current_user = nil
  	redirect_to "/login"
  end
end
