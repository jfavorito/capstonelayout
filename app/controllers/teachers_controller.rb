class TeachersController < ApplicationController
	include SessionsHelper
	before_action :require_login, :set_cache_buster	

	def profile
		@faculty = Faculty.find(current_user.id)
	end

	def mysubjects
		@faculty = Faculty.find(current_user.id)
		@arr = Array.new
		@toffering = Offering.where("slots_taken > ?", 0).where(faculty_id: current_user.id)
		@toffering.each do |o| 
			if !@arr.include? o.schoolyear_id
				@arr << o.schoolyear_id
			end
		end
		@schoolyear = Schoolyear.where(id: @arr).sort_by &:startyr
	end

	def subjectdetails
	    @offering = Offering.find(params[:id])
	    @toff = Subject.find(@offering.subject_id).subject_code
	    @recordlog = Recordlog.where(offering_id: params[:id])
	    @schedule = Schedule.where(offering_id: params[:id]).first
	    @arr = Array.new
	    @recordlog.each do |r|
	      @arr << r.history_id
	    end

	    @arr1 = Array.new
	    @histories = History.where(id: @arr)

	    @histories.each do |h|
	      @arr1 << h.student_id
	    end

	    @students = Student.where(id: @arr1).sort_by &:s_lname
	    @tempSem = Semester.find(@offering.semester_id)
	    @tempSy = Schoolyear.find(@offering.schoolyear_id)
	    @tempSubj = Subject.find(@offering.subject_id)
	    @tempFacul = Faculty.find(@offering.faculty_id)
	    @sched = Schedule.find_by(offering_id: @offering.id)
	    respond_to do |format|
	      format.html
	      format.pdf do
	        pdf = ClasslistPdf.new(@offering, @students)
	        send_data pdf.render, filename: "#{@toff}_#{@offering.section}.pdf",
	                              type: "application/pdf",
	                              disposition: "inline"
	      end
	        format.xlsx {
	          response.headers['Content-Disposition'] = 'attachment; filename="Classlist.xlsx"'
	        }
	    end
	end
	def compexam
		@compexamsubjs = Compexamsubj.where(examiner: current_user.id)
		@cexam = Array.new
		@compexamsubjs.each do |c|
			@cexam << c.compexam_id
		end
		@compexam = Compexam.where(id: @cexam).sort_by &:date_applied
	end
	def edit_subj
		@compexamsubj = Compexamsubj.find(params[:id])
		@offering = Offering.find(@compexamsubj.offering_id)
		@subject = Subject.find(@offering.subject_id)
		respond_to do |format|
			format.html
			format.js
		end
	end

	def update_subj
	    @compexamsubj = Compexamsubj.find(params[:id])
	    @compexamsubj.update_attributes(:passed => params[:compexamsubj][:passed])
	    @compexamsubj.update_attributes(:id => params[:id])
	    @compexamsubj.save
	end

	def edit_password
		@teacher = Faculty.find(current_user.id)
	end

	def update_password
		@teacher = Faculty.find(current_user.id)
		user = Faculty.find(current_user.id).try(:authenticate, params[:current_password])
		if user && @teacher.update_attributes(faculty_params)
			@teacher.save!
		    redirect_to '/teachers/dashboard'
		    gflash success: "Password updated"
		else
			redirect_to '/teachers/edit_password'
		end
	end

	private

	def faculty_params
		params.require(:faculty).permit(:password, :password_confirmation)
	end

  	def require_login
    	unless logged_in?
	      	flash[:error] = "Please log in to proceed"
	      	redirect_to login_url
	    	end
 	 end

  	def set_cache_buster
    	response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    	response.headers["Pragma"] = "no-cache"
    	response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  	end 	
end
