class StudentsController < ApplicationController
	include SessionsHelper
	before_action :require_login, :set_cache_buster
	def dashboard
	end

	def profile
		@student = Student.find(current_user.id)
	end

	def compexam
		@compexam = Compexam.where(student_id: current_user.id)
		@last = Compexam.where(student_id: current_user.id).last
		@hstry = History.where(student_id: current_user.id)
		@hsArr = Array.new
		@passed = 0
		@hstry.each do |hs|
			@hsArr << hs.id
		end
		@rlog = Recordlog.where(history_id: @hsArr).count
		@count = Compexam.where(student_id: current_user.id).count
		@ref = Compexam.where(student_id: current_user.id).first
		@student = Student.find(current_user.id)
		@checker = false
		@compexam.each do |comp|
		      @csb = Compexamsubj.where(compexam_id: comp.id).where(passed: true).count
		      @blank = Compexamsubj.where(compexam_id: comp.id).where(passed: nil).count
		      if @blank > 0
		        @checker = true
		      end
		      @passed = @passed + @csb			
		end
	end

	def apply
		@compexam = Compexam.new 
		@container = Array.new
	    @tempHis = History.where(student_id: current_user.id)
	    @tempHis.each do |temp|
	      @container << temp.id
	    end
	    @tempRec = Recordlog.where(history_id: @container)
	    @container2 = Array.new
	    @tempRec.each do |tempRec|
	      @container2 << tempRec.offering_id
	    end
	end

	def create_application
		@compexam = Compexam.new
		@compexam.student_id = current_user.id
		@student = Student.find(current_user.id)
		@compexam.or_no = params[:compexam][:or_no]
		@compexam.amount = params[:compexam][:amount]
		@temp = params[:compexam][:temp]
		@r = Array.new
	    @temp.each do |t|
	      if t.to_i > 0
	        @r << t.to_i
	      end
	    end
	    @compexam.subjtotake = Tempval.first.subjtotake
	    @compexam.subjtopass = Tempval.first.default_subjtopass
	    @compexam.date_applied = Time.now.to_date
	    if @r.size == Tempval.first.subjtotake && @compexam.save
		      @r.each do |off|
		          @compexamsubj = Compexamsubj.new
		          @compexamsubj.offering_id = off
		          @compexamsubj.compexam_id = Compexam.last.id
		          @compexamsubj.examiner = Offering.find(off).faculty_id
		          @compexamsubj.save
		      end
		      redirect_to '/students/compexam'
		      gflash success: "Successfully saved in the database!"
    	else
      		redirect_to '/students/compexam/apply',
      		danger: "Invalid sets of input"
	    end
	end

	def exam_details
		@compexamsubj = Compexamsubj.where(compexam_id: params[:id])
	end

	def enlist
	    @student = Student.find(current_user.id)
	    @tempArr = Array.new
	    @contpan = Contpan.first
	    @tempOffering = Offering.where(schoolyear_id: @contpan.schoolyear_id).where(semester_id: @contpan.semester_id)
	    @tempOffering.each do |t|
	      @tempArr << t.id
	    end
	    @schedule = Schedule.where(offering_id: @tempArr)

	    @query1 = History.where(student_id: current_user.id)
	    @arr = Array.new
	    @query1.each do |q|
	      @arr << q.id  
	    end
	    
	    if !History.where(student_id: current_user.id).where(sy: Contpan.first.schoolyear_id).where(sem: Contpan.first.semester_id).present?
	      @history = History.new
	      @history.student_id = current_user.id
	      @history.discount_id = Discount.find_by(disc_name: "None").id
	      @history.date_enlisted = Time.now.to_date
	      @history.sy = Contpan.first.schoolyear_id
      	  @history.sem =  Contpan.first.semester_id
	      @history.save
		 else
		  	@history
	    end
	    @hs = History.find_by(student_id: current_user.id, sy: @contpan.schoolyear_id, sem: @contpan.semester_id)
	    @recordlog = Recordlog.where(history_id: @hs)
	    @tof = Array.new
	    @rep = Array.new
	    @recordlog.each do |rec|
	 		@rep << rec.offering_id
	    end

	    @tos = Array.new
	    @reptemp = Offering.where(id: @rep)
	    @reptemp.each do |toof|
	      @tos << toof.subject_id
	    end

	    respond_to do |format|
	    	format.html
	    	format.pdf do
		        pdf = EnlistmentPdf.new(@student, @reptemp)
		        send_data pdf.render, filename: "EnlistedSubjects.pdf",
		                              type: "application/pdf",
		                              disposition: "inline"
	    	end
	    end
	end

	def edit_studenthistory
		@history = History.find(params[:id])
		respond_to do |format|
			format.html
			format.js
		end
	end

	def update_studenthistory
		@history = History.find(params[:id])
		@history.update_attributes(:discount_id => params[:history][:discount_id])
		@history.save
		redirect_to '/students/enlist'
		    gflash success: "Successfully saved in the database!"
	end

	def enlist_subject
	    @offering = Offering.find(params[:id])
	    @student = Student.find(current_user.id)
	    @studid = @student.stud_id
	    @student.update_attributes(:status => "Enrolled")
	    @student.update_attributes(:stud_id => @studid)
	    @student.save!
	    @count = @offering.slots_available - 1
	    @taken = @offering.slots_taken + 1
	    @temp = Time.now.to_date.to_s + " 00:00:00"
	    @history = History.where(sy: Contpan.first.schoolyear_id).where(sem: Contpan.first.semester_id).where(student_id: current_user.id).first
	    @offering.update_attributes(:slots_available => @count)
	    @offering.update_attributes(:slots_taken => @taken)
	    @ctype = Classtype.all
	    @ctype.each do |ct| 
	      if @taken >= ct.ctype_count && @taken <= ct.countmax
	        @offering.update_attributes(:classtype_id => ct.id)
	      end
	    end
	    @offering.save
	    @recordlog = Recordlog.new
	    @recordlog.offering_id = @offering.id
	    @recordlog.history_id = @history.id
	    @recordlog.save

	    redirect_to '/students/enlist'
	        gflash success: "Successfully saved in the database!"
	end

	def drop_subject
	    @recordlog = Recordlog.find(params[:id])
	    @offid = @recordlog.offering_id
	    @recordlog.destroy
	    @history = History.where(student_id: current_user.id).where(sy: @sy).where(sem: @sem)
	    @arr = Array.new
	    @history.each do |h|
	      @arr << h.id
	    end
	    @tempReclog = Recordlog.where(history_id: @arr).count
	    if @tempReclog <= 0
	      @student = Student.find(current_user.id)
	      @tempid2 = @student.id
	      @stud_id = @student.stud_id
	      @stat = "Not Enrolled"
	      @student.update_attributes(:stud_id => @stud_id)
	      @student.update_attributes(:status => @stat)
	    end
	    @offering = Offering.find(@offid)
	    @taken = @offering.slots_taken - 1
	    @offering.update_attributes(:slots_available => @offering.slots_available + 1)
	    @offering.update_attributes(:slots_taken => @taken)
	    @ctype = Classtype.all
	    @ctype.each do |ct| 
	      if @taken >= ct.ctype_count && @taken <= ct.countmax
	        @offering.update_attributes(:classtype_id => ct.id)
	      end
	    end    
	    @offering.save
	    @history = History.where(student_id: current_user.id)
	    redirect_to '/students/enlist'
	     gflash success: "Successfully dropped from the database!"
	end

	def mysubjects
	    @history = History.where(student_id: current_user.id)
	    @tempArr = Array.new
	    @syArr = Array.new
	    @history.each do |h|
	      @tempArr << h.id
	      if !@syArr.include? h.sy
	      	@syArr << h.sy
	      end
	    end
	    @schoolyear = Schoolyear.where(id: @syArr).sort_by &:startyr
	end
	def offering
			@tempArr = Array.new
		  	@temp = Offering.where(schoolyear_id: Contpan.first.schoolyear_id).where(semester_id: Contpan.first.semester_id)
		    @temp.each do |t|
		      @tempArr << t.id
		    end
		    if Contpan.first.enforce == true
		      @schedule = Schedule.where(offering_id: @tempArr)
		    else
		      @schedule = Schedule.all
		    end		
	end

	def retake_subj
		@compexam = Compexam.new
		@temp = Compexam.where(student_id: current_user.id).last.id
		@subj = Compexamsubj.where(compexam_id: @temp).where(passed: false)
		@student = Student.find(current_user.id)
		respond_to do |format|
			format.html
			format.js
		end
	end

	def confirm_retake
		@compexam = Compexam.new
		@compexam.or_no = params[:compexam][:or_no]
		@compexam.amount = params[:compexam][:amount]
		@compexam.student_id = current_user.id
		@compexam.date_applied = Time.now
		@temp = Compexam.where(student_id: current_user.id).last.id
		@passed = Compexamsubj.where(compexam_id: @temp).where(passed: true).count
		@subjs = Compexamsubj.where(compexam_id: @temp).where(passed: false)
		@count = Compexamsubj.where(compexam_id: @temp).where(passed: false).count
		@compexam.subjtotake = @count
		@compexam.subjtopass = Compexam.find(@temp).subjtopass - @passed
		@compexam.save
		@subjs.each do |s|
			@compexamsubj = Compexamsubj.new
			@compexam = Compexam.last.id
			@compexamsubj.offering_id = s.offering_id
			@compexamsubj.examiner = s.examiner
			@compexamsubj.compexam_id = @compexam
			@compexamsubj.save
		end
		redirect_to '/students/compexam'
      	gflash success: "Successfully saved in the database!"
	end

	def edit_password
		@student = Student.find(current_user.id)
	end

	def update_password
		@student = Student.find(current_user.id)
		student = Student.find(current_user.id).try(:authenticate, params[:current_password])
		if @student.update_attributes(student_params) && student
			@student.save
		    redirect_to '/students/dashboard'
		    gflash success: "Password updated"
		else
			redirect_to '/students/edit_password'
		end
	end

	private

	def student_params
		params.require(:student).permit(:password, :password_confirmation)
	end

  	def require_login
    	unless logged_in?
	      	flash[:error] = "Please login to proceed"
	      	redirect_to login_url
	    	end
 	 end

  	def set_cache_buster
    	response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    	response.headers["Pragma"] = "no-cache"
    	response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  	end  	
end
