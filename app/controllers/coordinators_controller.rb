class CoordinatorsController < ApplicationController
  include SessionsHelper
  authorize_resource :class => false
  before_action :require_login, :set_cache_buster

  # contents of the dashboard to be loaded
  # by calling Contpan, Tempval, and Appearance tables
  def dashboard
    @contpan = Contpan.all
    @tempval = Tempval.all
    @appearance = Appearance.all
    @appimage = Appearance.first
  end

  # Settings
  def settings
    @contpan = Contpan.all
    @tempval = Tempval.all
    @appearance = Appearance.all
    @appimage = Appearance.first
  end

  def help
  end

  def edit_contpan
    @contpan = Contpan.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_contpan
    @contpan = Contpan.find(params[:id])
    @contpan.update_attributes(:college => params[:contpan][:college])
    @contpan.update_attributes(:department => params[:contpan][:department])
    @contpan.update_attributes(:full_name => params[:contpan][:full_name])
    @contpan.update_attributes(:site_name => params[:contpan][:site_name])
    @contpan.update_attributes(:semester_id => params[:contpan][:semester_id])
    @contpan.update_attributes(:schoolyear_id => params[:contpan][:schoolyear_id])
    @contpan.save
    @student = Student.all
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    @student.each do |s|
        @tempStudent = Student.find(s.id)
        @studid = @tempStudent.stud_id
        @tempArr = Array.new
        @history = History.where(student_id: s.id).where(sem: @sem).where(sy: @sy)
        @history.each do |h|
          @tempArr << h.id
        end
        @recordlog = Recordlog.where(history_id: @tempArr)
        @tempArray = Array.new
        @recordlog.each do |r|
          @tempArray << r.offering_id
        end
        @offering = Offering.where(id: @tempArray).where(schoolyear_id: @sy).where(semester_id: @sem)
        if @offering.count <= 0
          @tempStudent.update_attributes(:status => "Not Enrolled")
          @tempStudent.update_attributes(:stud_id => @studid)
          @tempStudent.save!
        else
          @tempStudent.update_attributes(:status => "Enrolled")
          @tempStudent.update_attributes(:stud_id => @studid)
          @tempStudent.save!
        end
    end
      redirect_to '/coordinators/dashboard'
      gflash success: "Successfully saved in the database!"
  end

  def edit_tempval
    @tempval = Tempval.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_tempval
    @tempval = Tempval.find(params[:id])
    @tempval.update_attributes(:default_subjtopass => params[:tempval][:default_subjtopass])
    @tempval.update_attributes(:subjtotake => params[:tempval][:subjtotake])
    @tempval.save
    redirect_to '/coordinators/dashboard'
      gflash success: "Successfully saved in the database!"
  end

  def offering
    @tempArr = Array.new
    @sem = Contpan.first.semester_id
    @sy = Contpan.first.schoolyear_id
  	@temp = Offering.where(schoolyear_id: @sy).where(semester_id: @sem)
    @temp.each do |t|
      @tempArr << t.id
    end
    if Contpan.first.enforce == true
      @schedule = Schedule.where(offering_id: @tempArr)
    else
      @schedule = Schedule.all
    end
    @offobj = Offering.where(schoolyear_id: @sy).where(semester_id: @sem)
    respond_to do |format|
        format.html
        format.pdf do
          pdf = OfferingPdf.new(@offobj)
          send_data pdf.render, filename: "SubjectOfferings.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
    end
  end

  def add_offering
  	@offering = Offering.new
    1.times {@offering.schedules.build}
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_offering
    @offering = Offering.new(offering_params)
    @offering.fee_id = 1
    @offering.slots_taken = 0
    @offering.semester_id = Contpan.first.semester_id
    @offering.schoolyear_id = Contpan.first.schoolyear_id
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    if @offering.save
        @off = Offering.last
        @offids = Array.new
        @to = Offering.where(semester_id: @sem).where(schoolyear_id: @sy).where.not(id: @off.id)
        @to.each do |to|
          @offids << to.id
        end
        @conflict = false
        @room = Room.find(@off.room_id).room_name
        @faculty = Faculty.find(@off.faculty_id).f_fname
        @sched = Schedule.where(offering_id: @off.id)
        @tempschedule = Schedule.where(offering_id: @offids)
        @sched.each do |s|
          #@temp = Offering.find(s.offering_id)
          @tempschedule.each do |t|
            @temp = Offering.find(t.offering_id)
            if @off.room_id == @temp.room_id && Room.find(@off.room_id).room_name != "TBA" && ( (t.time_start <= s.time_start && t.time_end > s.time_start && s.days == t.days) || (t.time_start >= s.time_start && t.time_start > s.time_end && s.days == t.days ) )
              @conflict = true
              break
            end

            if @off.faculty_id == @temp.faculty_id && Faculty.find(@off.faculty_id).f_fname != "TBA" && ( (t.time_start <= s.time_start && t.time_end > s.time_start && s.days == t.days) || (t.time_start >= s.time_start && t.time_start > s.time_end && s.days == t.days ) )
              @conflict = true
              break
            end
          end
          if @conflict == true
            break
          end
        end
        #check for conflict

        if @conflict == true
          @off.destroy
          redirect_to '/coordinators/offering',
          danger: "Possible Conflict"
        else
          @def_fee = Subject.find(@off.subject_id).default_fee
          @units = Subject.find(@off.subject_id).units
          @id = Fee.find(@def_fee).id
          @off.update_attributes(:fee_id => @id)
          @off.update_attributes(:unit => @units)
          @off.save
          redirect_to '/coordinators/offering'
          gflash success: "Successfully saved in the database!"
        end
    else
        redirect_to '/coordinators/offering',
        danger: "Invalid input"
    end

  end

  def edit_offering
    @offering = Offering.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_offering
    @offering = Offering.find(params[:id])
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    @contSched = Schedule.where(offering_id: @offering.id)
    @start = Array.new
    @end = Array.new
    @days = Array.new
    @contSched.each do |c|
      @start << c.time_start
      @end << c.time_end
      @days << c.days
    end
    @rm = @offering.room_id
    @fac = @offering.faculty_id
    @fee = @offering.fee_id
    @sub = @offering.subject_id
    @ct = @offering.classtype_id
    @u = @offering.unit
    @sec = @offering.section
    @slo = @offering.slots_available
    if @offering.update_attributes(offering_params) && @offering.save
        @offids = Array.new
        @to = Offering.where(semester_id: @sem).where(schoolyear_id: @sy).where.not(id: @offering.id)
        @to.each do |to|
          @offids << to.id
        end
        @conflict = false
        @sched = Schedule.where(offering_id: @offering.id)
        @tempschedule = Schedule.where(offering_id: @offids)
        @sched.each do |s|
          @tempschedule.each do |t|
            @temp = Offering.find(t.offering_id)
            if @offering.room_id == @temp.room_id && Room.find(@offering.room_id).room_name != "TBA" && ( (t.time_start <= s.time_start && t.time_end > s.time_start && s.days == t.days) || (t.time_start >= s.time_start && t.time_start > s.time_end && s.days == t.days ) )
              @conflict = true
              break
            end

            if @offering.faculty_id == @temp.faculty_id && Faculty.find(@offering.faculty_id).f_fname != "TBA" && ( (t.time_start <= s.time_start && t.time_end > s.time_start && s.days == t.days) || (t.time_start >= s.time_start && t.time_start > s.time_end && s.days == t.days ) )
              @conflict = true
              break
            end
          end
        end 
        if @conflict == true
            @count = 0
            @checker = false
            @contSched.each do |c|
              @tempCont = Schedule.find(c.id)
              @tempCont.update_attributes(:time_start => @start[@count])
              @tempCont.update_attributes(:time_end => @end[@count])
              @tempCont.update_attributes(:days => @days[@count])
              if @tempCont.save
                @checker = true
              end
              @count = @count + 1
            end
            @offering.update_attributes(:room_id => @rm)
            @offering.update_attributes(:faculty_id => @fac)
            @offering.update_attributes(:fee_id => @fee)
            @offering.update_attributes(:subject_id => @sub)
            @offering.update_attributes(:classtype_id => @ct)
            @offering.update_attributes(:unit => @u)
            @offering.update_attributes(:section => @sec)
            @offering.update_attributes(:slots_available => @slo)
            if @offering.save && @checker == true
              redirect_to '/coordinators/offering', danger: "Possible Conflict"
            end
        else
            redirect_to '/coordinators/offering'
            gflash success: "Walang conflict"
        end
    else
      redirect_to '/coordinators/offering', danger: "Invalid Input"
    end
  end

  def delete_offering
    @offering = Offering.find(params[:id])
    @compexamsubj = Compexamsubj.where(offering_id: params[:id]).count
    if @compexamsubj > 0
      redirect_to '/coordinators/offering',
      danger: 'Contains Model Connections'
    else
      @offering.destroy
      redirect_to '/coordinators/offering'
      gflash success: "Successfully deleted from the database!"
    end
  end
  
  # Lines 287 - 338
  # CRUD Functions (Create, Read, Update, Delete)
  # for Programs
  def programs
    @program = Program.all
    @try = false
  end

  def add_program
    @program = Program.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def delete_program
    @program = Program.find(params[:id])
    @program.destroy
    redirect_to '/coordinators/programs'
    gflash success: "Successfully deleted from the database!"
  end

  def edit_program
    @program = Program.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_program
    @program = Program.find(params[:id])
    @program.update_attributes(:program_title => params[:program][:program_title])
    if @program.save!
      redirect_to '/coordinators/programs'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/programs',
        danger: "Invalid Input"
    end
  end

  def create_program
    @program = Program.new
    @program.program_title = params[:program][:program_title]

    if @program.save
      redirect_to '/coordinators/programs'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/programs',
        danger: "Invalid Input"
    end
  end

  # Lines 341 - 396
  # CRUD Functions (Create, Read, Update, Delete)
  # for Specialization
  def specialization
    @specialization = Specialization.all
  end

  def add_specialization
    @specialization = Specialization.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_specialization
    @specialization = Specialization.new
    @specialization.spec_name = params[:specialization][:spec_name]
    @specialization.program_id = params[:specialization][:program_id]

    if @specialization.save
      redirect_to '/coordinators/specialization'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/specialization',
        danger: "Invalid Input"
    end
  end

  def edit_specialization
    @specialization = Specialization.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_specialization
    @specialization = Specialization.find(params[:id])
    @specialization.update_attributes(:spec_name => params[:specialization][:spec_name])
    @specialization.update_attributes(:program_id => params[:specialization][:program_id])

    if @specialization.save
      redirect_to '/coordinators/specialization'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/specialization',
        danger: "Invalid Input"
    end
  end

  def delete_specialization
    @specialization = Specialization.find(params[:id])
    @specialization.destroy
    redirect_to '/coordinators/specialization'
    gflash success: "Successfully deleted from the database!"
  end

  # Lines 401 - 452
  # CRUD Functions (Create, Read, Update, Delete)
  # for Curriculum
  def curriculum
    @curriculum = Curriculum.all
  end

  def add_curriculum
    @curriculum = Curriculum.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_curriculum
    @curriculum = Curriculum.new
    @curriculum.curr_type = params[:curriculum][:curr_type]

    if @curriculum.save
      redirect_to '/coordinators/curriculum'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/curriculum',
        danger: "Invalid Input"
    end
  end

  def edit_curriculum
    @curriculum = Curriculum.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_curriculum
    @curriculum = Curriculum.find(params[:id])
    @curriculum.update_attributes(:curr_type => params[:curriculum][:curr_type])

    if @curriculum.save
      redirect_to '/coordinators/curriculum'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/curriculum',
        danger: "Invalid Input"
    end
  end

  def delete_curriculum
    @curriculum = Curriculum.find(params[:id])
    @curriculum.destroy
    redirect_to '/coordinators/curriculum'
    gflash success: "Successfully deleted from the database!"
  end
  # Lines 457 - 550
  # CRUD Functions (Create, Read, Update, Delete) and
  # generating PDF files
  # for Students
  def student
    if Contpan.first.enforce == true
      @tempstudent = Student.where(status: "Enrolled")
    else
      @tempstudent = Student.all
    end
    @student = Student.all
    respond_to do |format|
      format.html
      format.pdf do
          pdf = StudentsPdf.new(@tempstudent)
          send_data pdf.render, filename: "students.pdf",
                                type: "application/pdf",
                                disposition: "inline"
      end
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="students.xlsx"'
      }
    end
  end

  def add_student
    @student = Student.new
    @yr = Array.new
    @yr << 1
    @yr << 2
    @yr << 3
    @yr << 4
    @yr << 5
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_student
    @student = Student.new
    @student.stud_id = params[:student][:stud_id]
    @student.s_fname = params[:student][:s_fname]
    @student.s_mname = params[:student][:s_mname]
    @student.s_lname = params[:student][:s_lname]
    @student.year    = params[:student][:year]
    @student.gender  = params[:student][:gender]
    @student.status  = "Not Enrolled"
    @student.username = "s"+ params[:student][:stud_id]
    @student.password = params[:student][:s_lname] + params[:student][:s_fname][0..3]
    @student.specialization_id = params[:student][:specialization_id]
    @student.curriculum_id = params[:student][:curriculum_id]

    if @student.save
      redirect_to '/coordinators/student'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/student',
        danger: "Invalid Input"
    end
  end

  def edit_student
    @student = Student.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_student
    @student = Student.find(params[:id])
    @student.update_attributes(:stud_id => params[:student][:stud_id])
    @student.update_attributes(:s_fname => params[:student][:s_fname])
    @student.update_attributes(:s_mname => params[:student][:s_mname])
    @student.update_attributes(:s_lname => params[:student][:s_lname])
    @student.update_attributes(:gender => params[:student][:gender])
    @student.update_attributes(:curriculum_id => params[:student][:curriculum_id])
    @student.update_attributes(:specialization_id => params[:student][:specialization_id])
   # @user = "s" + params[:student][:stud_id]
    #@student.update_attributes(:username)
    if @student.save

      redirect_to '/coordinators/student'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/student',
        danger: "Invalid Input"
    end
  end

  def delete_student
    @student = Student.find(params[:id])
    @student.destroy

    redirect_to '/coordinators/student'
      gflash success: "Successfully deleted from the database!"
  end

  # Lines 555 - 645
  # CRUD Functions (Create, Read, Update, Delete)
  # for Faculty
  def faculty
    @faculty = Faculty.where.not(f_fname: "TBA")
  end

  def add_faculty
    @faculty = Faculty.new
    @status = Array.new
    @status << "Part-time"
    @status << "Full-time"
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_faculty
    @faculty = Faculty.new
    @faculty.f_id = params[:faculty][:f_id]
    @faculty.f_fname = params[:faculty][:f_fname]
    @faculty.f_mname = params[:faculty][:f_mname]
    @faculty.f_lname = params[:faculty][:f_lname]
    @faculty.f_type = params[:faculty][:f_type]
    @faculty.username = "t"+ params[:faculty][:f_id]
    @faculty.password = params[:faculty][:f_lname]+ params[:faculty][:f_fname][0..3]
    @faculty.degree = params[:faculty][:degree]
    if @faculty.save
      @user = User.new
      @user.username = "c" + params[:faculty][:f_id]
      @user.password = params[:faculty][:f_lname]+ params[:faculty][:f_fname][0..3]
      @user.admin = false
      @user.role = 3
      @user.save
      redirect_to '/coordinators/faculty'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/faculty',
        danger: "Invalid Input"
    end
  end

  def edit_faculty
    @faculty = Faculty.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_faculty
    @faculty = Faculty.find(params[:id])
    @faculty.update_attributes(:f_id => params[:faculty][:f_id])
    @faculty.update_attributes(:f_fname => params[:faculty][:f_fname])
    @faculty.update_attributes(:f_mname => params[:faculty][:f_mname])
    @faculty.update_attributes(:f_lname => params[:faculty][:f_lname])
    @faculty.update_attributes(:f_type => params[:faculty][:f_type])
    @faculty.update_attributes(:degree => params[:faculty][:degree])

    if @faculty.save
      redirect_to '/coordinators/faculty'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/faculty',
        danger: "Invalid Input"
    end
  end

  def delete_faculty
    @faculty = Faculty.find(params[:id])
    @username = "c" + @faculty.f_id
    @tba = Faculty.find_by(f_fname: "TBA")
    @user = User.find_by(username: @username)
    @user.destroy
    @offering = Offering.where(faculty_id: @faculty.id)
    @offering.each do |o|
      @tempOff = Offering.find(o.id)
      @tempOff.update_attributes(:faculty_id => @tba.id)
      @tempOff.update_attributes(:id => o.id)
      @tempOff.save
    end
    @compexamsubj = Compexamsubj.where(examiner: @faculty.id)
    @compexamsubj.each do |c|
      @temp = Compexamsubj.find(c.id)
      @temp.update_attributes(:examiner => @tba.id)
      @temp.update_attributes(:id => @temp.id)
      @temp.save
    end
    @faculty.destroy

    redirect_to '/coordinators/faculty'
      gflash success: "Successfully deleted from the database!"
  end

  # Lines 650 - 705
  # CRUD Functions (Create, Read, Update, Delete)
  # for Subject
  def subject
    @subject = Subject.all
  end

  def add_subject
    @subject = Subject.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_subject
    @subject = Subject.new
    @subject.subject_code = params[:subject][:subject_code]
    @subject.subject_title = params[:subject][:subject_title]
    @subject.default_fee = params[:subject][:default_fee]
    @subject.units = params[:subject][:units]
    if @subject.save
      redirect_to '/coordinators/subject'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/subject',
        danger: "Invalid Input"
    end
  end

  def edit_subject
    @subject = Subject.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_subject
    @subject = Subject.find(params[:id])
    @subject.update_attributes(:subject_code => params[:subject][:subject_code])
    @subject.update_attributes(:subject_title => params[:subject][:subject_title])
    @subject.update_attributes(:default_fee => params[:subject][:default_fee])
    @subject.update_attributes(:units => params[:subject][:units])
    if @subject.save
      redirect_to '/coordinators/subject'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/subject',
        danger: "Invalid Input"
    end
  end

  def delete_subject
    @subject = Subject.find(params[:id])
    @subject.destroy
    redirect_to '/coordinators/subject'
      gflash success: "Successfully deleted from the database!"
  end
  # Lines 709 - 766
  # CRUD Functions (Create, Read, Update, Delete)
  # for Room
  def room
    @room = Room.where.not(room_name: "TBA")
  end

  def add_room
    @room = Room.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_room
    @room = Room.new
    @room.room_name = params[:room][:room_name]

    if @room.save
      redirect_to '/coordinators/room'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/room',
        danger: "Invalid Input"
    end
  end

  def edit_room
    @room = Room.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end
  def update_room
    @room = Room.find(params[:id])
    @room.update_attributes(:room_name => params[:room][:room_name])
    if @room.save
      redirect_to '/coordinators/room'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/room',
        danger: "Invalid Input"
    end
  end

  def delete_room
    @room = Room.find(params[:id])
    @tba = Room.find_by(room_name: "TBA")
    @offering = Offering.where(room_id: @room.id)
    @offering.each do |o|
      @tempOff = Offering.find(o.id)
      @tempOff.update_attributes(:room_id => @tba.id)
      @tempOff.update_attributes(:id => o.id)
      @tempOff.save
    end
    @room.destroy
    redirect_to '/coordinators/room'
      gflash success: "Successfully deleted from the database!"
  end

  # Lines 771 - 824
  # CRUD Functions (Create, Read, Update, Delete)
  # for Classtype
  def classtype
    @classtype = Classtype.all
  end

  def add_classtype
    @classtype = Classtype.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_classtype
    @classtype = Classtype.new
    @classtype.ctype_name = params[:classtype][:ctype_name]
    @classtype.ctype_count = params[:classtype][:ctype_count]
    @classtype.countmax = params[:classtype][:countmax]
    if @classtype.save
      redirect_to '/coordinators/classtype'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/classtype',
        danger: "Invalid Input"
    end
  end

  def edit_classtype
    @classtype = Classtype.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_classtype
    @classtype = Classtype.find(params[:id])
    @classtype.update_attributes(:ctype_name => params[:classtype][:ctype_name])
    @classtype.update_attributes(:ctype_count => params[:classtype][:ctype_count])
    @classtype.update_attributes(:countmax => params[:classtype][:countmax])
    if @classtype.save
      redirect_to '/coordinators/classtype'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/classtype',
        danger: "Invalid Input"
    end
  end

  def delete_classtype
    @classtype = Classtype.find(params[:id])
    @classtype.destroy
    redirect_to '/coordinators/classtype'
    gflash success: "Successfully deleted from the database!"
  end
  # Lines 828 - 879
  # CRUD Functions (Create, Read, Update, Delete)
  # for Fee
  def fee
    @fee = Fee.where.not(fee_name: "None")
  end

  def add_fee
    @fee = Fee.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_fee
    @fee = Fee.new
    @fee.fee_name = params[:fee][:fee_name]

    if @fee.save
      redirect_to '/coordinators/fee'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/fee',
        danger: "Invalid Input"
    end
  end

  def edit_fee
    @fee = Fee.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_fee
    @fee = Fee.find(params[:id])
    @fee.update_attributes(:fee_name => params[:fee][:fee_name])

    if @fee.save
      redirect_to '/coordinators/fee'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/fee',
        danger: "Invalid Input"
    end
  end

  def delete_fee
    @fee = Fee.find(params[:id])
    @fee.destroy
    redirect_to '/coordinators/fee'
    gflash success: "Successfully deleted from the database!"
  end
  # Lines 883 - 934
  # CRUD Functions (Create, Read, Update, Delete)
  # for Semester
  def semester
    @semester = Semester.all
  end

  def add_semester
    @semester = Semester.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_semester
    @semester = Semester.new
    @semester.sem_name = params[:semester][:sem_name]

    if @semester.save
      redirect_to '/coordinators/semester'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/semester',
        danger: "Invalid Input"
    end
  end

  def edit_semester
    @semester = Semester.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_semester
    @semester = Semester.find(params[:id])
    @semester.update_attributes(:sem_name => params[:semester][:sem_name])

    if @semester.save
      redirect_to '/coordinators/semester'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/semester',
        danger: "Invalid Input"
    end
  end

  def delete_semester
    @semester = Semester.find(params[:id])
    @semester.destroy
    redirect_to '/coordinators/semester'
    gflash success: "Successfully deleted from the database!"
  end
  # Lines 938 -991
  # CRUD Functions (Create, Read, Update, Delete)
  # for School Year
  def schoolyear
    @schoolyear = Schoolyear.all
  end

  def add_schoolyear
    @schoolyear = Schoolyear.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_schoolyear
    @schoolyear = Schoolyear.new
    @schoolyear.startyr = params[:schoolyear][:startyr]
    @schoolyear.endyear = params[:schoolyear][:endyear]

    if @schoolyear.save
      redirect_to '/coordinators/schoolyear'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/schoolyear',
        danger: "Invalid Input"
    end
  end

  def edit_schoolyear
    @schoolyear = Schoolyear.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_schoolyear
    @schoolyear = Schoolyear.find(params[:id])
    @schoolyear.update_attributes(:startyr => params[:schoolyear][:startyr])
    @schoolyear.update_attributes(:endyear => params[:schoolyear][:endyear])

    if @schoolyear.save
      redirect_to '/coordinators/schoolyear'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/schoolyear',
        danger: "Invalid Input"
    end
  end

  def delete_schoolyear
    @schoolyear = Schoolyear.find(params[:id])
    @schoolyear.destroy
    redirect_to '/coordinators/schoolyear'
    gflash success: "Successfully deleted from the database!"
  end

  def enlist
    @student = Student.all
  end

  # function to enlist a student
  def enlist_student
    @student = Student.find(params[:id])
    if Contpan.first.enforce == true
      @tempArr = Array.new
      @contpan = Contpan.first
      @tempOffering = Offering.where(schoolyear_id: @contpan.schoolyear_id).where(semester_id: @contpan.semester_id)
      @tempOffering.each do |t|
        @tempArr << t.id
      end
      @schedule = Schedule.where(offering_id: @tempArr)
    else
      @schedule = Schedule.all
    end
    @query1 = History.where(student_id: params[:id])
    @arr = Array.new
    @query1.each do |q|
      @arr << q.id
    end
    if !History.where(student_id: params[:id]).where(sy: Contpan.first.schoolyear_id).where(sem: Contpan.first.semester_id).present?
      @history = History.new
      @history.student_id = params[:id]
      @history.discount_id = Discount.find_by(disc_name: "None").id
      @history.date_enlisted = Time.now.to_date
      @history.sy = Contpan.first.schoolyear_id
      @history.sem =  Contpan.first.semester_id
      @history.save
    else
      @history
    end
    @hs = History.find_by(student_id: params[:id], sy: @contpan.schoolyear_id, sem: @contpan.semester_id).id
    @recordlog = Recordlog.where(history_id: @hs)
    @tof = Array.new
    @rep = Array.new
    @recordlog.each do |rec|
      @rep << rec.offering_id
    end
    @tos = Array.new
    @reptemp = Offering.where(id: @rep) #for reports
    @reptemp.each do |toof|
      @tos << toof.subject_id
    end

    respond_to do |format|
      format.html
      format.pdf do
        pdf = EnlistmentPdf.new(@student, @reptemp)
        send_data pdf.render, filename: "EnlistedSubjects.pdf",
                              type: "application/pdf",
                              disposition: "inline"
      end
    end
  end

  # function to enlist subjects
  # to a student
  def enlist_subject
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    @adv = Classtype.find_by(ctype_name: "ADVISEMENT")
    @offering = Offering.find(params[:sid])
    @student = Student.find(params[:id])
    @idnum = @student.stud_id
    @stat = "Enrolled"
    @student.update_attributes(:stud_id => @idnum)
    @student.update_attributes(:status => @stat)
    @student.save!
    @count = @offering.slots_available - 1
    @taken = @offering.slots_taken + 1
    @temp = Time.now.to_date.to_s + " 00:00:00"
    @history = History.where(sy: @sy).where(sem: @sem).where(student_id: params[:id]).first
    @offering.update_attributes(:slots_available => @count)
    @offering.update_attributes(:slots_taken => @taken)
    @ctype = Classtype.all
    @ctype.each do |ct|
      if @offering.classtype_id == @adv.id
        break
      elsif @taken >= ct.ctype_count && @taken <= ct.countmax
        @offering.update_attributes(:classtype_id => ct.id)
      end
    end
    @offering.save!
    @recordlog = Recordlog.new
    @recordlog.offering_id = @offering.id
    @recordlog.history_id = @history.id
    @recordlog.save
    @history.update_attributes(:date_enlisted=> Time.now)
    @history.update_attributes(:student_id => params[:id])
    @history.save
    redirect_to enlistment_path(:id => @student.id)
    gflash success: "Successfully saved in the database!"
  end

  # drop an enlisted subject
  def drop_subject
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    @recordlog = Recordlog.find(params[:sid])
    @offid = @recordlog.offering_id
    @recordlog.destroy
    @history = History.where(student_id: params[:id]).where(sy: @sy).where(sem: @sem)
    @arr = Array.new
    @history.each do |h|
      @arr << h.id
    end
    @tempReclog = Recordlog.where(history_id: @arr).count
    if @tempReclog <= 0
      @student = Student.find(params[:id])
      @tempid2 = @student.id
      @stud_id = @student.stud_id
      @stat = "Not Enrolled"
      @student.update_attributes(:stud_id => @stud_id)
      @student.update_attributes(:status => @stat)
    end
    @offering = Offering.find(@offid)
    @taken = @offering.slots_taken - 1
    @offering.update_attributes(:slots_available => @offering.slots_available + 1)
    @offering.update_attributes(:slots_taken => @taken)
    @ctype = Classtype.all
    @ctype.each do |ct|
      if @taken >= ct.ctype_count && @taken <= ct.countmax
        @offering.update_attributes(:classtype_id => ct.id)
      end
    end
    @offering.save

    redirect_to enlistment_path(:sid => @tempid2)
    gflash success: "Successfully dropped from the database!"
  end

  # function to get the details of the
  # subject offering like schedule, students, and
  # enlisted
  def offering_details
    @offering = Offering.find(params[:id])
    @toff = Subject.find(@offering.subject_id).subject_code
    @recordlog = Recordlog.where(offering_id: params[:id])
    @schedule = Schedule.where(offering_id: params[:id]).first
    @arr = Array.new
    @recordlog.each do |r|
      @arr << r.history_id
    end

    @arr1 = Array.new
    @histories = History.where(id: @arr)

    @histories.each do |h|
      @arr1 << h.student_id
    end

    @students = Student.where(id: @arr1).sort_by &:s_lname
    @tempSem = Semester.find(@offering.semester_id)
    @tempSy = Schoolyear.find(@offering.schoolyear_id)
    @tempSubj = Subject.find(@offering.subject_id)
    @tempFacul = Faculty.find(@offering.faculty_id)
    @sched = Schedule.find_by(offering_id: @offering.id)
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ClasslistPdf.new(@offering, @students)
        send_data pdf.render, filename: "#{@toff}_#{@offering.section}.pdf",
                              type: "application/pdf",
                              disposition: "inline"
      end
      format.xlsx {
            response.headers['Content-Disposition'] = 'attachment; filename="Classlist.xlsx"'
          }
    end
  end

  # function to view all comprehensive exams
  def compexam
    @student = Student.all
    @sids = Array.new
    @student.each do |s|
      @history = History.where(student_id: s.id)
      @tArr = Array.new
      @history.each do |h|
        @tArr << h.id
      end
      @tempReclog = Recordlog.where(history_id: @tArr)
      if @tempReclog.count >= Tempval.first.subjtotake
          @sids << s.id
      end
    end

    @students = Student.where(id: @sids)
  end

  # function for applying for a comprehensive 
  # exams
  def apply
    @compexam = Compexam.new
    @student = Student.find(params[:id])
    @temp = Tempval.first
    @container = Array.new
    @tempHis = History.where(student_id: params[:id])
    @tempHis.each do |temp|
      @container << temp.id
    end
    @tempRec = Recordlog.where(history_id: @container)
    @container2 = Array.new
    @tempRec.each do |tempRec|
      @container2 << tempRec.offering_id
    end
  end

  def create_application
    @compexam = Compexam.new
    @compexam.student_id = params[:compexam][:student_id]
    @student = Student.find(params[:id])
    @compexam.or_no = params[:compexam][:or_no]
    @compexam.amount = params[:compexam][:amount]
    @temp = params[:compexam][:temp]
    @r = Array.new
    @temp.each do |t|
      if t.to_i > 0
        @r << t.to_i
      end
    end
    @compexam.subjtotake = Tempval.first.subjtotake
    @compexam.subjtopass = Tempval.first.default_subjtopass
    @compexam.date_applied = Time.now
    if @r.size == Tempval.first.subjtotake && @compexam.save
      @r.each do |off|
          @compexamsubj = Compexamsubj.new
          @compexamsubj.offering_id = off
          @compexamsubj.compexam_id = Compexam.last.id
          @compexamsubj.examiner = Offering.find(off).faculty_id
          @compexamsubj.save
      end
      redirect_to '/coordinators/compexamapps'
      gflash success: "Successfully saved in the database!"
    else
      redirect_to n_application_path(:id => @student.id), danger: "\tYOU DID NOT SELECT THE NUMBER OF SUBJECTS"
    end
  end

  # comprehensive exam applications
  def compexamapps
    @compexam = Compexam.all.sort_by(&:date_applied)
  end

  # details of the comprehensive exam which includes
  # the subjects, result, and number of subjects a student
  # needs to pass
  def exam_details
    @compexamsubj = Compexamsubj.where(compexam_id: params[:id])
    @tempcompexam = Compexam.find(params[:id]).student_id
    @student = Student.find(@tempcompexam)
    @failCount = Compexamsubj.where(compexam_id: params[:id]).where(passed: false).count
    @diff = Tempval.first.subjtotake - Tempval.first.default_subjtopass
  end

  def edit_compexam
    @compexam = Compexam.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end
  def edit_compexamsubj
    @compexamsubj = Compexamsubj.find(params[:id])
    @container = Array.new
    @tempHis = History.where(student_id: Compexam.find(@compexamsubj.compexam_id).student_id)
    @tempHis.each do |temp|
      @container << temp.id
    end
    @tempRec = Recordlog.where(history_id: @container)
    @container2 = Array.new
    @tempRec.each do |tempRec|
      @container2 << tempRec.offering_id
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_compexam
    @compexam = Compexam.find(params[:id])
    @compexam.update_attributes(:date_applied => params[:compexam][:date_aplied])
    @compexam.update_attributes(:subjtotake => params[:compexam][:subjtotake])
    @compexam.update_attributes(:subjtopass => params[:compexam][:subjtopass])
    @compexam.update_attributes(:student_id => params[:compexam][:student_id])
    @compexam.save
  end
  def update_compexamsubj
    @compexamsubj = Compexamsubj.find(params[:id])
    @compexamsubj.update_attributes(:offering_id => params[:compexamsubj][:offering_id])
    @compexamsubj.update_attributes(:datetaken => params[:compexamsubj][:datetaken])
    @compexamsubj.update_attributes(:examiner => params[:compexamsubj][:examiner])
    @compexamsubj.update_attributes(:passed => params[:compexamsubj][:passed])
    @compexamsubj.update_attributes(:sched_date => params[:compexamsubj][:sched_date])
    @compexamsubj.update_attributes(:sched_time => params[:compexamsubj][:sched_time])
    @compexamsubj.save
  end

  def discount
    @discount = Discount.where.not(disc_name: "None")
  end

  # for retaking a failed comprehensive exam subject
  def add_retake
    @compexam = Compexam.new
    @subj = Compexamsubj.where(compexam_id: params[:id]).where(passed: false)
    @c = Compexam.find(params[:id])
    @student = Student.find(@c.student_id)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_retake
    @compexam = Compexam.new
    @compexam.or_no = params[:compexam][:or_no]
    @compexam.amount = params[:compexam][:amount]
    @passed = Compexamsubj.where(compexam_id: params[:id]).where(passed: true).count
    @subjs = Compexamsubj.where(compexam_id: params[:id]).where(passed: false)
    @count = Compexamsubj.where(compexam_id: params[:id]).where(passed: false).count
    @cexam = Compexam.find(params[:id])
    @compexam.date_applied = Time.now.to_date
    @compexam.student_id = @cexam.student_id
    @compexam.subjtotake = @count
    @compexam.subjtopass = @cexam.subjtopass - @passed
    @compexam.save
    @subjs.each do |s|
      @cs = Compexamsubj.find(s.id)
      @compexamsubj = Compexamsubj.new
      @compexamsubj.offering_id = @cs.offering_id
      @compexamsubj.compexam_id = Compexam.last.id
      @compexamsubj.examiner = Offering.find(@cs.offering_id).faculty_id
      @compexamsubj.save
    end
    redirect_to '/coordinators/compexam'
      gflash success: "Successfully saved in the database!"
  end
  # Lines 1338 - 1396
  # CRUD Functions (Create, Read, Update, Delete)
  # for Discount
  def add_discount
      @discount = Discount.new
      respond_to do |format|
      format.html
      format.js
      end
  end

  def create_discount
    @discount = Discount.new
    @discount.disc_name = params[:discount][:disc_name]
    @discount.disc_value = params[:discount][:disc_value]

    if @discount.save
      redirect_to '/coordinators/discount'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/discount',
        danger: "Invalid Input"
    end
  end

  def edit_discount
    @discount = Discount.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_discount
    @discount = Discount.find(params[:id])
    @discount.update_attributes(:disc_name => params[:discount][:disc_name])
    @discount.update_attributes(:disc_value => params[:discount][:disc_value])

    if @discount.save
      redirect_to '/coordinators/discount'
        gflash success: "Successfully saved in the database!"
    else
      redirect_to '/coordinators/discount',
        danger: "Invalid Input"
    end
  end

  def delete_discount
    @discount = Discount.find(params[:id])
    @history = History.where(discount_id: params[:id])
    @history.each do |h|
      @temp = History.find(h.id)
      @id = @temp.id
      @tempDis = Discount.find_by(disc_name: "None")
      @temp.update_attributes(:id => @id)
      @temp.update_attributes(:discount_id => @tempDis.id)
      @temp.save
    end
    @discount.destroy
    redirect_to '/coordinators/discount'
    gflash success: "Successfully deleted from the database!"
  end
  # Adding/Updating a student's tuition
  # fee discount
  def edit_history
    @history = History.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_history
    @history = History.find(params[:id])
    @history.update_attributes(:discount_id => params[:history][:discount_id])
    @history.save
  end
  # Lines 1415 - 1462
  # Report Generation for Students
  # with tuition fee discount
  def tuition
    @tempD = Discount.where.not(disc_name: "None")
    @idarr = Array.new
    @tempD.each do |temp|
      @idarr << temp.id
    end
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    @history = History.where(discount_id: @idarr).where(sem: @sem).where(sy: @sy)

    @studentArray = Array.new
    @history.each do |htemp|
      @tempStudent = Student.find(htemp.student_id)
      if @tempStudent.status == "Enrolled"
        @studentArray << htemp.student_id
      end
    end
    @tstu = Student.where(id: @studentArray).sort_by &:s_lname

    @historytemp1 = History.where(discount_id: @idarr).where(sem: @sem).where(sy: @sy).where(student_id: @studentArray)
    @q = History.where(discount_id: @idarr)
    respond_to do |format|
        format.html
        format.pdf do
          pdf = TuitionPdf.new(@tstu)
          send_data pdf.render, filename: "TuitionFeeDiscounts.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
        format.xlsx {
          response.headers['Content-Disposition'] = 'attachment; filename="tuitionfeedsicounts.xlsx"'
        }
    end
  end

  def edit_tfeediscount
    @history = History.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_tfeediscount
    @history = History.find(params[:id])
    @history.update_attributes(:discount_id => params[:history][:discount_id])
    @history.save
  end
  # Lines 1466 - 1503
  # Report Generation for Subjects with or
  # without Room Fees
  def roomandfee
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    if Contpan.first.enforce == true
      @offering = Offering.where("slots_taken > ?", 0).where(schoolyear_id: @sy).where(semester_id: @sem)
    else
      @offering = Offering.where("slots_taken > ?", 0)
    end
    respond_to do |format|
      format.html
      format.pdf do
        pdf = RoomfeePdf.new(@offering)
        send_data pdf.render, filename: "RoomsandFees.pdf",
                              type: "application/pdf",
                              disposition: "inline"
      end
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="roomsandfees.xlsx"'

      }
    end
  end

  def edit_roomfee
    @offering = Offering.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_roomfee
    @offering = Offering.find(params[:id])
    @offering.update_attributes(:fee_id => params[:offering][:fee_id])
    @offering.save
    redirect_to '/coordinators/roomandfee'
      gflash success: "Successfully saved in the database!"
  end
  # Generation of Faculty Loading Report
  def facultyloading
    if Contpan.first.enforce == true
      @sem = Contpan.first.semester_id
      @sy = Contpan.first.schoolyear_id
      @tempOff = Offering.where("slots_taken > ?", 0).where(schoolyear_id: @sy).where(semester_id: @sem)
    else
      @tempOff = Offering.where("slots_taken > ?", 0)
    end
    @tempArr = Array.new
    @tempOff.each do |temp|
      if !@tempArr.include? temp.faculty_id
        @tempArr << temp.faculty_id
      end
    end
    @faculty = Faculty.where(id: @tempArr).sort_by &:f_lname
    respond_to do |format|
        format.html
        format.pdf do
          pdf = FacultyloadingPdf.new(@faculty)
          send_data pdf.render, filename: "FacultyLoading.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
        format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="facultyloading.xlsx"'

        }
    end
  end
  # Getting the subjects enlisted by a student
  def studentsubjects
    @student = Student.find(params[:id])
    @history = History.where(student_id: params[:id])
    @tempArr = Array.new
    @history.each do |h|
      @tempArr << h.id
    end
    @record = Recordlog.where(history_id: @tempArr)
    @tempArr2 = Array.new
    @record.each do |r|
      @tempArr2 << r.offering_id
    end
    @offering = Offering.where(id: @tempArr2)
  end
  # Generation of Enrollment Data
  def enrollmentdata
    @curriculum = Curriculum.all
    @program = Program.all
    @sy = Contpan.first.schoolyear_id
    @sem = Contpan.first.semester_id
    if Contpan.first.enforce == true
      @offering = Offering.where(schoolyear_id: @sy).where(semester_id: @sem)
    else
      @offering = Offering.all
    end
    @tempArr = Array.new
    @offering.each do |o|
      @tempArr << o.id
    end
    @recordlog = Recordlog.where(offering_id: @tempArr)
    @tempArr2 = Array.new
    @recordlog.each do |r|
      @tempArr2 << r.history_id
    end
    @history = History.where(id: @tempArr2)
    @tempArr3 = Array.new
    @history.each do |h|
      @tempArr3 << h.student_id
    end
    respond_to do |format|
      format.html
      format.pdf do
          pdf = EnrollmentdataPdf.new(@tempArr3)
          send_data pdf.render, filename: "EnrollmentData.pdf",
                                type: "application/pdf",
                                disposition: "inline"
      end
      format.xlsx {
        response.headers['Content-Disposition'] = 'attachment; filename="enrollmentdata.xlsx"'

      }
    end
  end
  # function for retaking a comprehensive exam
  def retake
    @subjs = Compexamsubj.where(compexam_id: params[:sid]).where(passed: false)
    @passed = Compexamsubj.where(compexam_id: params[:sid]).where(passed: true).count
    @cnt = Compexamsubj.where(compexam_id: params[:sid]).where(passed: false).count
    @cexam =  Compexam.find(params[:sid])
    @compexam = Compexam.new
    @compexam.or_no = @cexam.or_no
    @compexam.amount = @cexam.amount
    @compexam.student_id = @cexam.student_id
    @compexam.date_applied = Time.now
    @compexam.subjtotake = @cnt
    @compexam.subjtopass = @cexam.subjtopass - @passed
    @compexam.save

    @subjs.each do |s|
      @cs = Compexamsubj.find(s.id)
      @compexamsubj = Compexamsubj.new
      @compexamsubj.offering_id = @cs.offering_id
      @compexamsubj.compexam_id = Compexam.last.id
      @compexamsubj.examiner = Offering.find(@cs.offering_id).faculty_id
      @compexamsubj.save
    end

  end
  # retrieving info about a student
  # and exams taken
  def student_exams
    @student = Student.find(params[:id])
    @compexam = Compexam.where(student_id: params[:id])
    @ref = Compexam.where(student_id: params[:id]).first
    @last = Compexam.where(student_id: params[:id]).last
    @passed = 0
    @checker = false
    @compexam.each do |comp|
      @csb = Compexamsubj.where(compexam_id: comp.id).where(passed: true).count
      @blank = Compexamsubj.where(compexam_id: comp.id).where(passed: nil).count
      if @blank > 0
        @checker = true
      end
      @passed = @passed + @csb
    end
  end
  # Lines 1633 - 1662
  # CRUD for Site Appearance
  def add_appearance
    @appearance = Appearance.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create_appearance
    @appearance = Appearance.new
    @appearance.color = params[:appearance][:color]
    @appearance.image = params[:appearance][:image]
    @appearance.save
  end

  def edit_appearance
    @appearance = Appearance.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_appearance
    @appearance = Appearance.find(params[:id])
    @appearance.update_attributes(:color => params[:appearance][:color])
    @appearance.update_attributes(:sec_color => params[:appearance][:sec_color])
    @appearance.update_attributes(:image => params[:appearance][:image])
    @appearance.save
  end

  def edit_password
    @user = User.find(current_user.id)
  end

  def update_password
    @user = User.find(current_user.id)
    user = User.find(current_user.id).try(:authenticate, params[:current_password])
    if user && @user.update_attributes(user_params)
      @user.save
      redirect_to '/coordinators/dashboard'
      gflash success: "Password updated"
    else
      redirect_to cedit_password_path
      flash.now[:error] = "Incorrect current password"
    end
  end

  def classlist
    @schedule = Schedule.all
  end

  private

  def offering_params
    params.require(:offering).permit(:section, :slots_available,:faculty_id, :room_id,:classtype_id,:subject_id, schedules_attributes: [:id, :time_start, :time_end, {:days => []}, :offering_id])
  end

  def appearance_params
    params.require(:appearance).permit(:color, :image)
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def require_login
    unless logged_in?
      flash[:error] = "Please login to proceed"
      redirect_to login_url
    end
  end

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
  # def tempsched
  #   @tempsched = Tempsched.all
  # end

  # def add_tempsched
  #   @tempsched = Tempsched.new
  #   respond_to do |format|
  #     format.html
  #     format.js
  #   end
  # end

  # def create_tempsched
  #   @tempsched = Tempsched.new
  #   @tempsched.t_start = params[:tempsched][:t_start]
  #   @tempsched.t_end = params[:tempsched][:t_end]
  #   @tempsched.t_days = params[:tempsched][:t_days]
  #   @tempsched.save
  #   redirect_to '/coordinators/tempsched'
  #     gflash success: "Successfully saved in the database!"
  # end