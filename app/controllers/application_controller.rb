class ApplicationController < ActionController::Base
	 include SessionsHelper
	 protect_from_forgery with: :exception
	 add_flash_types :danger, :info, :warning, :success

	rescue_from CanCan::AccessDenied do |exception|
    	redirect_to '/sessions/new', :alert => exception.message
  	end
end
