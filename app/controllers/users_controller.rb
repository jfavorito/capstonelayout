class UsersController < ApplicationController
  include SessionsHelper
  before_action :require_login, :set_cache_buster	
  def home
  	@user = User.where.not(id: current_user.id)
  end

  def edit_user
  	@user = User.find(params[:id])
  	@role = Array.new
  	@role << "Coordinator"
  	@role << "Teacher"
  	respond_to do |format|
  		format.html
  		format.js
  	end
  end

  def update_user
  	@user = User.find(params[:id])
  	@user.update_attributes(:username => params[:user][:username])
  	@user.update_attributes(:role => params[:user][:role])
  	@user.save!  
  	redirect_to '/users/home'
  end

  def edit_password
    @user = User.find(current_user.id)
  end

  def update_password
    @user = User.find(current_user.id)
    user = User.find(current_user.id).try(:authenticate, params[:current_password])
    if user && @user.update_attributes(user_params)
      @user.save
      redirect_to '/users/home'
      gflash success: "Password updated"
    else
      redirect_to 'users/edit_password'
      flash.now[:error] = "Incorrect current password"
    end
  end
  private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end
  	def require_login
    	unless logged_in?
	      	flash[:error] = "Hello"
	      	redirect_to login_url
	    	end
 	 end

  	def set_cache_buster
    	response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    	response.headers["Pragma"] = "no-cache"
    	response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  	end 
end
