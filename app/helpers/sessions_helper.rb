module SessionsHelper
	def login(user, type)
		session[:user_id] = user.id
		session[:user_type] = type
	end

	def current_user
		if session[:user_type] == "student"
			@current_user ||= Student.find_by(id: session[:user_id])
		elsif session[:user_type] == "faculty"
			@current_user ||= Faculty.find_by(id: session[:user_id])
		else
			@current_user ||= User.find_by(id: session[:user_id])
		end
	end

	def log_out
		session.delete(:user_id)
		@current_user = nil
	end

	def logged_in?
		!current_user.nil?
	end
end
