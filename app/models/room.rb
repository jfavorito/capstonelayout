class Room < ApplicationRecord
	has_many :offerings, dependent: :destroy

	#  Line 8-10
	#  Validates the room_name's presence, length(only 4-13 characters), format,
	#  and its uniqueness

	validates :room_name, presence: true, length: { minimum: 4, maximum: 13 }
	validates_format_of :room_name, :with => /\A[a-z0-9]+\z/i
	validates :room_name, :uniqueness => true

	#  Line 15-19
	#  Capitalizes all lowercase characters before saving

	before_save :uppercase_room_name

	def uppercase_room_name
    room_name.upcase!
  end
end
