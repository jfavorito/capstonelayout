class Schedule < ApplicationRecord
	belongs_to :offering
	validates :time_start, presence: true
	validates :time_end, presence: true

  #  Line 9-10
	#  Checks if the time_start and time_end are present

	validates :time_start, presence: true
	validates :time_end, presence: true

	#  Line 15
	#  Days must be present

	validates_format_of :days, :without => /\A(["0", "0", "0", "0", "0", "0", "0", "0", "0"])\Z/
end
