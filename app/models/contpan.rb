class Contpan < ApplicationRecord
	belongs_to :semester
	belongs_to :schoolyear

	#  Line 9-10
	#  Validates site_name's presence, length(up to 35 characters),
	#  and its format(letters)

	validates :site_name, presence: true, length: { maximum: 35 }
	validates_format_of :site_name, :multiline => true, :with => /\A[a-z ]+\z/i
end
