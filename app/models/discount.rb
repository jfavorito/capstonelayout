class Discount < ApplicationRecord
	has_many :histories

	#  Line 7
	#  Validates disc_value's presence and format

	validates :disc_value, presence: true, :numericality => { :less_than => 1.01 }

	#  Line 12-14
	#  Validates disc_name's precence, length(atleast 2 characters), format, and its uniqueness

	validates :disc_name, presence: true, length: { minimum: 2 }
	validates_format_of :disc_name, :multiline => true, :with => /^\A[%,&-a-z0-9 ]+\z$/i
	validates :disc_name, :uniqueness => true

end
