class Specialization < ApplicationRecord
	belongs_to :program
	has_many :student, dependent: :destroy

	#  Line 9-11
	#  Validates the spec_name's presence if it is true, length with atleast
	#  5 characters, format, and its uniqueness before saving

	validates :spec_name, presence: true, length: { minimum: 5 }
	validates_format_of :spec_name, :multiline => true, :with => /^\A[,-a-z0-9 ]+\z$/i
	validates :spec_name, :uniqueness => true
end
