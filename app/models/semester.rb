class Semester < ApplicationRecord
	has_many :offerings, dependent: :destroy

    #  Line 8-10
	#  Validates the sem_name's presence, length with atleast 3 characters,
	#  format, and its uniqueness before saving

	validates_format_of :sem_name, :multiline => true, :with => /\A[a-z0-9 ]+\z/i
	validates :sem_name, presence: true, length: { minimum: 3 }
	validates :sem_name, :uniqueness => true

    #  Line 14-19
	#  Capitalizes the first character in every word on sem_name before saving

	before_save :capitalize_sem_name

	def capitalize_sem_name
	  self.sem_name = self.sem_name.split.collect(&:capitalize).join(' ') if self.sem_name && !self.sem_name.blank?
	end
end
