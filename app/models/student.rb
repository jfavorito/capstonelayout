class Student < ApplicationRecord
	belongs_to :specialization
	belongs_to :curriculum
	has_many :histories, dependent: :destroy
	has_secure_password

	# Line 10-12
	# Validates the stud_id's presence, length with only 9 characters, format,
	# and its uniqueness
	validates :stud_id, presence: true, length: { minimum: 9, maximum: 9 }
	validates_format_of :stud_id, :with => /[0-9]/
	validates :stud_id, :uniqueness => true

	# Line 16-17
	# Validates the s_fname's precence, format, and length with only 1-30 characters
	validates :s_fname, presence: true, length: { minimum: 1, maximum: 30 }
	validates_format_of :s_fname, :multiline => true, :with => /^\A[a-z ]+\z$/i

	# Line 21
	# Validates the s_lname's precence and its length with only 1-30 characters
	validates :s_lname, presence: true, length: { minimum: 1, maximum: 30 }

	# Line 25
	# Validates year, curriculum_id, and specialization_id
	validates :year, :curriculum_id, :specialization_id, :presence => true

	# Line 30-36
	# Capitalizes the first letter oof every word on s_fname, s_mname, and s_lname

	before_save :uppercase_name

	def uppercase_name
		self.s_fname = self.s_fname.split.collect(&:capitalize).join(' ') if self.s_fname && !self.s_fname.blank?
		self.s_mname = self.s_mname.split.collect(&:capitalize).join(' ') if self.s_mname && !self.s_mname.blank?
		self.s_lname = self.s_lname.split.collect(&:capitalize).join(' ') if self.s_lname && !self.s_lname.blank?
	end

end
