class User < ApplicationRecord
	has_secure_password

	#  Line 7-8
	#  Checks the password if it is correct

	validates :password, presence: true, length: {minimum: 5}, :if => :password
	validates_confirmation_of :password
end
