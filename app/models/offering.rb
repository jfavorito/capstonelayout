class Offering < ApplicationRecord
	has_many :schedules, dependent: :destroy, inverse_of: :offering
	accepts_nested_attributes_for :schedules
	has_many :histories, :through => :recordlogs
	has_many :recordlogs, dependent: :destroy
	has_many :compexamsubjs, dependent: :destroy
	belongs_to :semester
	belongs_to :subject
	belongs_to :schoolyear
	belongs_to :fee
	belongs_to :faculty
	belongs_to :classtype
	belongs_to :room

	#  Line 19-24
	#  Validates the precence of classtype_id, room_id, subject_id, faculty_id,
	#  section and its length(only 2-4 digits) and format(numbers and letters)

	validates :section, presence: true, length: { minimum: 2, maximum: 4 }
	validates_format_of :section, :with => /\A[a-z0-9]+\z/i
	validates :classtype_id, presence: true
	validates :room_id, presence: true
	validates :subject_id, presence: true
	validates :faculty_id, presence: true

  # Line 29-33
  # Capitalizes all lowercase characters on section before saving

	before_save :uppercase_section

	def uppercase_section
    section.upcase!
  end
end
