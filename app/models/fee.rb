class Fee < ApplicationRecord
	has_many :offerings, dependent: :destroy

  #  Line 8-10
	#  Validates the fee_name's presence, length(atleast 4 characters),
	#  format(letters), and its uniqueness

	validates :fee_name, presence: true, length: { minimum: 4 }
	validates_format_of :fee_name, :multiline => true, :with => /\A[a-z ]+\z/i
	validates :fee_name, :uniqueness => true
end
