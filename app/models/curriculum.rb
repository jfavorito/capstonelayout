class Curriculum < ApplicationRecord
	has_many :student, dependent: :destroy

	#  Line
	#  Validates curr_type's presence, length(atleast 3 characters),
	#  format(letters and numbers), and its uniqueness

	validates :curr_type, presence: true, length: { minimum: 3 }
	validates_format_of :curr_type, :multiline => true, :with => /^\A[a-z0-9 ]+\z$/i
	validates :curr_type, :uniqueness => true

	#  Line
	#  Capitalizes all lowercase on curr_type before saving

	before_save :uppercase_curr_type

	def uppercase_curr_type
    curr_type.upcase!
  end
end
