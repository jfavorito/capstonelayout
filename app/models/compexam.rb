class Compexam < ApplicationRecord
	belongs_to :student
	has_many :compexamsubjs, dependent: :destroy, inverse_of: :compexam
	accepts_nested_attributes_for :compexamsubjs

	#  Line 9-10
	#  Validates or_no's presence, length(only 6 digits), and its format(numbers)

	validates :or_no, presence: true, length: { minimum: 6, maximum: 6 }
	validates_format_of :or_no, :with => /[0-9]/

	#  Line 15
	#  Validates amount's presence and format(numeric)

	validates :amount, :presence => true, numericality: true
end
