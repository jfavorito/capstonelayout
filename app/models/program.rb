class Program < ApplicationRecord
	has_many :specialization, dependent: :destroy

    #  Line 6-7
	#  Validates the program_title's presence, length(atleast 3 characters), and format
	validates :program_title, presence: true, length: { minimum: 3 }
	validates_format_of :program_title, :multiline => true, :with => /\A[a-z ]+\z/i

end
