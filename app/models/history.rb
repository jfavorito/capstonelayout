class History < ApplicationRecord
	belongs_to :student
	belongs_to :discount
	has_many :offerings, through: :recordlog, dependent: :destroy
	has_many :recordlog, dependent: :destroy
end
