class Classtype < ApplicationRecord
	has_many :offerings, dependent: :destroy

	#  Line 8-10
	#  Validates ctype_name's presence, length(atleast 5 characters),
	#  format(alphanumeric), and its uniqueness

	validates :ctype_name, presence: true, length: { minimum: 5 }
	validates_format_of :ctype_name, :multiline => true, :with => /^\A[a-z0-9 ]+\z$/i
	validates :ctype_name, :uniqueness => true

	#  Line 15-16
	#  Validates ctype_count's precence, length(1-2 digits), and its format(numeric)

	validates :ctype_count, presence: true, length: { minimum: 1, maximum: 2 }
	validates_format_of :ctype_count, :with => /[0-9]/

	#  Line 21-22
	#  Validates countmax's precence, length(1-2 digits), and its format(numeric)

	validates :countmax, presence: true, length: { minimum: 1, maximum: 2 }
	validates_format_of :countmax, :with => /[0-9]/

	#  Line 28-29
	#  Validates ctype_count, greater than 0 and less than or equal to countmax
    #  Validates countmax, less than or equal to 99

	validates_numericality_of :ctype_count, greater_than: 0, less_than_or_equal_to: :countmax
	validates_numericality_of :countmax, less_than_or_equal_to: 99

	#  Line 34-38
	#  Capitalizes all lowercase on ctype_name before saving

	before_save :uppercase_ctype_name

	def uppercase_ctype_name
		ctype_name.upcase!
	end
end
