class Subject < ApplicationRecord
	has_many :offerings, dependent: :destroy

	#  Line 8-10
	#  Validates the incoming Subject code's length, format, and uniqueness
	#  before saving to the Subject table

	validates :subject_code, presence: true, length: { minimum: 5, maximum: 7 }
	validates_format_of :subject_code, :with => /\A[a-z0-9]+\z/i
	validates :subject_code, :uniqueness => true

    #  Line 16-18
	#  Validates the incoming Subject title's length, format, and uniqueness
	#  before saving to the Subject table

	validates :subject_title, presence: true, length: { minimum: 3 }
	validates_format_of :subject_title, :multiline => true, :with => /^\A[.,&-a-z0-9 ]+\z$/i
	validates :subject_title, :uniqueness => true
	before_save :uppercase_subject_code

	#  Line 23-27
	#  Changes the lowercase characters to uppercase before saving


	def uppercase_subject_code
		subject_code.upcase!
	end

	#  Line 32-38
	#  Capitalizes the first character in every word on subject_title before saving

	before_save :capitalize_subject_title

	def capitalize_subject_title
	  self.subject_title = self.subject_title.split.collect(&:capitalize).join(' ') if self.subject_title && !self.subject_title.blank?
	end
end
