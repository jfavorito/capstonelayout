class Faculty < ApplicationRecord
	has_many :offerings, dependent: :destroy
	has_secure_password

	#  Line 9-11
	#  Validates the f_id's presence, length(only 9 digits),
	#  format(numbers), and its uniqueness

	validates :f_id, presence: true, length: { minimum: 9, maximum: 9 }
	validates_format_of :f_id, :with => /[0-9]/
	validates :f_id, :uniqueness => true

	#  Line 16-17
	#  Validates the f_fname's presence, length(1-30 characters), and its format(letters)

	validates :f_fname, presence: true, length: { minimum: 1, maximum: 30 }
	validates_format_of :f_fname, :multiline => true, :with => /^\A[a-z ]+\z$/i

	#  Line 22-23
	#  Validates the f_mname's presence, length(1-25 characters), and its format(letters)

	validates :f_mname, presence: true, length: { minimum: 1, maximum: 25 }
	validates_format_of :f_mname, :multiline => true, :with => /^\A[a-z ]+\z$/i

	#  Line 28
	#  Validates the f_lname's presence, length(1-30 characters), and its format(letters)

	validates :f_lname, presence: true, length: { minimum: 1, maximum: 30 }

	#  Line 33-34
	#  Validates the degree's presence, length(atleast 7 characters), and its format(letters)

	validates :degree, presence: true, length: {minimum: 7}
	validates_format_of :degree, :multiline => true, :with => /^\A[a-z ]+\z$/i

	#  Line 39
	#  Validates the presence of f_type

	validates :f_type, presence: true

	#  Line 45-53
	#  Capitalizes the first letter of every word on
	#  f_fname, f_mname, f_lname, and degree before saving

	before_save :uppercase_name

	def uppercase_name
		self.f_fname = self.f_fname.split.collect(&:capitalize).join(' ') if self.f_fname && !self.f_fname.blank?
		self.f_mname = self.f_mname.split.collect(&:capitalize).join(' ') if self.f_mname && !self.f_mname.blank?
		self.f_lname = self.f_lname.split.collect(&:capitalize).join(' ') if self.f_lname && !self.f_lname.blank?

		self.degree = self.degree.split.collect(&:capitalize).join(' ') if self.degree && !self.degree.blank?
	end

end
