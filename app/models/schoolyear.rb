class Schoolyear < ApplicationRecord
	has_many :offerings, dependent: :destroy

    #  Line 8-12
	#  Validates startyr and endyear's precense, length(with only 4 characters),
	#  format(numbers), and its uniqueness

	validates :startyr, presence: true, length: { minimum: 4, maximum: 4 }
	validates_format_of :startyr, :with => /[0-9]/
	validates :endyear, presence: true, length: { minimum: 4, maximum: 4 }
	validates_format_of :endyear, :with => /[0-9]/
	validates :startyr, :endyear, :uniqueness => true

	#  Line 18-19
	#  Only allows input to startyr if it is less than endyear and only allows
	#  input to endyear if it is greater than startyr

	validates_numericality_of :startyr, less_than: :endyear
	validates_numericality_of :endyear, greater_than: :startyr

end
