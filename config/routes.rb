Rails.application.routes.draw do

  get 'coordinators/settings'
  get 'coordinators/help'
  get 'students/help'

  get 'users/home'

  get 'sessions/new'
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'
  delete '/logout' => 'sessions#destroy'

  get 'coordinators/edit_password' => 'coordinators#edit_password', :as => :cedit_password
  patch 'coordinators/update_password' => 'coordinators#update_password', :as => :update_password

  get 'teachers/edit_password' => 'teachers#edit_password'
  patch 'teachers/update_password' => 'teachers#update_password', :as => :update_passwordt

  get 'students/edit_password' => 'students#edit_password'
  patch 'students/update_password' => 'students#update_password', :as => :update_passwords

  get 'users/edit_password' => 'users#edit_password'
  patch 'users/update_password' => 'users#update_password', :as => :update_passwordu
  get 'teachers/profile' => 'teachers#profile'
  get 'students/profile' => 'students#profile'

  get 'coordinators/edit_compexam' => 'coordinators#edit_compexam', :as => :edit_compexam
  patch 'coordinators/update_compexam' => 'coordinators#update_compexam', :as => :update_compexam

  get 'coordinators/add_appearance' => 'coordinators#add_appearance', :as => :add_appearance
  post 'appearance' => 'coordinators#create_appearance'
  get 'coordinators/edit_appearance' => 'coordinators#edit_appearance', :as => :edit_appearance
  patch 'coordinators/update_appearance' => 'coordinators#update_appearance', :as => :update_appearance

  get 'coordinators/discount'
  get 'coordinators/add_discount' => 'coordinators#add_discount', :as => :add_discount
  post 'discount' => 'coordinators#create_discount'
  get 'coordinators/edit_discount' => 'coordinators#edit_discount', :as => :edit_discount
  patch 'coordinators/update_discount' => 'coordinators#update_discount', :as => :update_discount
  get 'coordinators/discount/:id/delete' => 'coordinators#delete_discount'

  get 'coordinators/dashboard'
  get 'coordinators/edit_contpan' => 'coordinators#edit_contpan', :as => :edit_contpan
  patch 'coordinators/edit_contpan' => 'coordinators#update_contpan',:as => :update_contpan
  get 'coordinators/edit_tempval' => 'coordinators#edit_tempval', :as => :edit_tempval
  patch 'coordinators/update_tempval' => 'coordinators#update_tempval', :as => :update_tempval

  get 'coordinators/studentsubjects/:id' => 'coordinators#studentsubjects'
  get 'coordinators/offering'
  get 'coordinators/compexam'

  get 'coordinators/offering/:id/delete' => 'coordinators#delete_offering'

  get 'coordinators/enrollmentdata'
  get 'coordinators/tuition'
  get 'coordinators/roomandfee'
  get 'coordinators/facultyloading'
  #get 'coordinators/:sid/retake/:id' => 'coordinators#retake'

  get 'coordinators/add_retake' => 'coordinators#add_retake', :as => :add_retake
  post 'compexam' => 'coordinators#create_retake', :as => :create_retake

  get 'coordinators/edit_history' => 'coordinators#edit_history', :as => :edit_history
  patch 'coordinators/update_history' => 'coordinators#update_history', :as => :update_history

  get 'coordinators/add_offering' => 'coordinators#add_offering', :as => :add_offering
  post 'offering' => 'coordinators#create_offering'
  get 'coordinators/offering/:id' => 'coordinators#offering_details'
  get 'coordinators/edit_offering' => 'coordinators#edit_offering', :as => :edit_offering
  patch 'coordinators/update_offering' => 'coordinators#update_offering', :as => :update_offering

  get 'coordinators/edit_tfeediscount' => 'coordinators#edit_tfeediscount', :as => :edit_tfeediscount
  patch 'coordinators/update_tfeediscount' => 'coordinators#update_tfeediscount', :as => :update_tfeediscount

  get 'coordinators/edit_roomfee' => 'coordinators#edit_roomfee', :as => :edit_roomfee
  patch 'coordinators/update_roomfee' => 'coordinators#update_roomfee', :as => :update_roomfee
  # programs table
  get 'coordinators/programs'
  get 'coordinators/add_program' => 'coordinators#add_program', :as => :add_program
  post 'programs' => 'coordinators#create_program'
  get 'coordinators/edit_program' => 'coordinators#edit_program', :as => :edit_program
  patch 'coordinators/update_program' => 'coordinators#update_program', :as => :update_program
  get 'coordinators/programs/:id/delete' => 'coordinators#delete_program'
  # specializations
  get 'coordinators/specialization'
  get 'coordinators/add_specialization' => 'coordinators#add_specialization', :as => :add_specialization
  post 'specialization' => 'coordinators#create_specialization'
  get 'coordinators/edit_specialization' => 'coordinators#edit_specialization', :as => :edit_specialization
  patch 'coordinators/update_specialization' => 'coordinators#update_specialization', :as => :update_specialization
  get 'coordinators/specialization/:id/delete' => 'coordinators#delete_specialization'

  get 'coordinators/curriculum'
  get 'coordinators/add_curriculum' => 'coordinators#add_curriculum', :as => :add_curriculum
  post 'curriculum' => 'coordinators#create_curriculum'
  get 'coordinators/edit_curriculum' => 'coordinators#edit_curriculum', :as => :edit_curriculum
  patch 'coordinators/update_curriculum' => 'coordinators#update_curriculum', :as => :update_curriculum
  get 'coordinators/curriculum/:id/delete' => 'coordinators#delete_curriculum'

  get 'coordinators/student'
  get 'coordinators/add_student' => 'coordinators#add_student', :as => :add_student
  post 'student' => 'coordinators#create_student'
  get 'coordinators/edit_student' => 'coordinators#edit_student', :as => :edit_student
  patch 'coordinators/update_student' => 'coordinators#update_student', :as => :update_student
  get 'coordinators/student/:id/delete' => 'coordinators#delete_student'

  get 'coordinators/faculty'
  get 'coordinators/add_faculty' => 'coordinators#add_faculty', :as => :add_faculty
  post 'faculty' => 'coordinators#create_faculty'
  get 'coordinators/edit_faculty' => 'coordinators#edit_faculty', :as => :edit_faculty
  patch 'coordinators/update_faculty' => 'coordinators#update_faculty', :as => :update_faculty
  get 'coordinators/faculty/:id/delete' => 'coordinators#delete_faculty'

  get 'coordinators/subject'
  get 'coordinators/add_subject' => 'coordinators#add_subject', :as => :add_subject
  post 'subject' => 'coordinators#create_subject'
  get 'coordinators/edit_subject' => 'coordinators#edit_subject', :as => :edit_subject
  patch 'coordinators/update_subject' => 'coordinators#update_subject', :as => :update_subject
  get 'coordinators/subject/:id/delete' => 'coordinators#delete_subject'

  get 'coordinators/room'
  get 'coordinators/add_room' => 'coordinators#add_room', :as => :add_room
  post 'room' => 'coordinators#create_room'
  get 'coordinators/edit_room' => 'coordinators#edit_room', :as => :edit_room
  patch 'coordinators/update_room' => 'coordinators#update_room', :as => :update_room
  get 'coordinators/room/:id/delete' => 'coordinators#delete_room'

  get 'coordinators/classtype'
  get 'coordinators/add_classtype' => 'coordinators#add_classtype', :as => :add_classtype
  post 'classtype' => 'coordinators#create_classtype'
  get 'coordinators/edit_classtype' => 'coordinators#edit_classtype', :as => :edit_classtype
  patch 'coordinators/update_classtype' => 'coordinators#update_classtype', :as => :update_classtype
  get 'coordinators/classtype/:id/delete' => 'coordinators#delete_classtype'

  get 'coordinators/fee'
  get 'coordinators/add_fee' => 'coordinators#add_fee', :as => :add_fee
  post 'fee' => 'coordinators#create_fee'
  get 'coordinators/edit_fee' => 'coordinators#edit_fee', :as => :edit_fee
  patch 'coordinators/update_fee' => 'coordinators#update_fee', :as => :update_fee
  get 'coordinators/fee/:id/delete' => 'coordinators#delete_fee'

  get 'coordinators/semester'
  get 'coordinators/add_semester' => 'coordinators#add_semester', :as => :add_semester
  post 'semester' => 'coordinators#create_semester'
  get 'coordinators/edit_semester' => 'coordinators#edit_semester', :as => :edit_semester
  patch 'coordinators/update_semester' =>'coordinators#update_semester', :as => :update_semester
  get 'coordinators/semester/:id/delete' => 'coordinators#delete_semester'

  get 'coordinators/tempsched'
  get 'coordinators/add_tempsched' => 'coordinators#add_tempsched', :as => :add_tempsched
  post 'tempsched' => 'coordinators#create_tempsched'

  get 'coordinators/schoolyear'
  get 'coordinators/add_schoolyear' => 'coordinators#add_schoolyear', :as => :add_schoolyear
  post 'schoolyear' => 'coordinators#create_schoolyear'
  get 'coordinators/edit_schoolyear' => 'coordinators#edit_schoolyear', :as => :edit_schoolyear
  patch 'coordinators/update_schoolyear' => 'coordinators#update_schoolyear', :as => :update_schoolyear

  get 'coordinators/schoolyear'
  get 'coordinators/schoolyear/:id/delete' => 'coordinators#delete_schoolyear'


  get 'coordinators/compexam/student_exam/exam_details/:id' => 'coordinators#exam_details'
  get 'coordinators/compexam'
  get 'coordinators/compexam/student_exams/:id' => 'coordinators#student_exams'
  get 'coordinators/compexam/apply/:id' => 'coordinators#apply', :as => :n_application
  post 'compexamapps' => 'coordinators#create_application', :as => :new_application

  get 'coordinators/compexamapps'
  get 'coordinators/classlist'
  
  get 'coordinators/edit_compexamsubj' => 'coordinators#edit_compexamsubj', :as => :edit_compexamsubj
  patch 'coordinators/update_compexamsubj' => 'coordinators#update_compexamsubj', :as => :update_compexamsubj

  get 'coordinators/enlist'
  get 'coordinators/:id' => 'coordinators#enlist_student', :as => :enlistment
  get 'coordinators/:sid/enlist_subject/:id' => 'coordinators#enlist_subject', :as => :cenlist_subject
  get 'coordinators/:sid/drop/:id' => 'coordinators#drop_subject'

  get 'students/dashboard'
  get 'students/compexam'
  get 'students/compexam/apply' => 'students#apply', :as => :s_application
  post 'compexamS' => 'students#create_application'
  get 'students/compexam/:id' => 'students#exam_details'

  get 'students/mysubjects'
  get 'students/offering'

  get 'students/enlist'

  get 'students/edit_studenthistory' => 'students#edit_studenthistory', :as => :edit_studenthistory
  patch 'students/update_studenthistory' => 'students#update_studenthistory', :as => :update_studenthistory

  get 'students/drop_subject/:id' => 'students#drop_subject'
  get 'students/enlist_subject/:id' => 'students#enlist_subject'


  get 'students/retake_subj' => 'students#retake_subj', :as => :retake_subj
  post 'students/compexam' => 'students#confirm_retake', :as => :confirm_retake


  get 'teachers/dashboard'
  get 'teachers/mysubjects'
  get 'teachers/mysubjects/:id' => 'teachers#subjectdetails'
  get 'teachers/compexam'
  get 'teachers/edit_subj' => 'teachers#edit_subj', :as => :edit_subj
  patch 'teachers/update_subj' => 'teachers#update_subj', :as => :update_subj

  get 'users/home'
  get 'users/edit_user' => 'users#edit_user', :as => :edit_user
  patch 'users/update_user' => 'users#update_user', :as => :update_user
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
